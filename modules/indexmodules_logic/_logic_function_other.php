<?

function GetBreadcrumbsArr($act,$id)
{
    global $par;

    $resarr = Array();
    $tablename = '';
    foreach($par->params AS $key=>$value)
    {
        if($value['actname']==$act) { $tablename = $value['tablename']; break; }
    }

    $pid = $id;
    $k=0;
    while($pid!=0)
    {
        $k++;
        if($k>10) break; //защита от вечного цикла, на всякий случай ;)
        $sql = "SELECT * FROM $tablename WHERE `id`=$pid";
        $res = mysql_query($sql);
        if($line = mysql_fetch_array($res,MYSQL_ASSOC))
        {
            $line = LangProcess($line);
            $item_url = GetSeoUrl($act,$line['id'],$line);
            $item_title = $line['title'];
            $resarr[] = Array('item_title'=>$item_title , 'item_url'=>$item_url);
            $pid = $line['parentid'];
        }
        else break;
    }

    $sql = "SELECT * FROM $par->topmenutable WHERE `url`='/$act'";
    $res = mysql_query($sql);
    if($line = mysql_fetch_array($res,MYSQL_ASSOC))
    {
        $item_url = GetSeoUrl($act,$line['id'],$line);
        $item_title = $line['title'];
        $resarr[] = Array('item_title'=>$item_title , 'item_url'=>$item_url);
    }

    if($act=="tovar")
    {
        $sql = "SELECT * FROM $par->objectstable WHERE `id`=$id";
        $res = mysql_query($sql);
        if($line = mysql_fetch_array($res,MYSQL_ASSOC))
        {
            $resarr = array_merge($resarr, GetBreadcrumbsArr('cat',$line['categid']) );
        }
    }

    return $resarr;
}

?>