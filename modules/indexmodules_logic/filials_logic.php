<?
    //филиалы имеют структуру Страна-город-филиал

    $filialsarr = Array();
    $filialsarr['title'] = 'Филиалы';
    
    
    $filialsarr['mainitem']['id'] = 0;
    $filialsarr['mainitem']['title'] = '';
    $filialsarr['mainitem']['lng'] = 0;
    $filialsarr['mainitem']['lat'] = 0;
    $filialsarr['mainitem']['zoom'] = 3;
    
    $filialsarr['items'] = Array();
    
    //определим заголовок и текст пункта
    if($id==0) $sql = "SELECT * FROM $par->topmenutable WHERE url='/filials'";
    else $sql = "SELECT * FROM $par->filialstable WHERE `id`=$id AND `hide`=0";
    $res = mysql_query($sql);
    if($line = mysql_fetch_array($res,MYSQL_ASSOC))
    {
        $line = LangProcess($line);
        $filialsarr['title'] = $line['title'];
        $filialsarr['text'] = $line['text'];
    }

    //список подпунктов данного пункта
    $sql = "SELECT * FROM $par->filialstable WHERE `parentid`=$id AND `hide`=0";
    //echo $sql.'<BR>';
    $res = mysql_query($sql);
    $k = 0; $slng = 0; $slat = 0;
    if(mysql_num_rows($res)>0)
    {
        while($line = mysql_fetch_array($res,MYSQL_ASSOC))
        {
            $k++;
            $a = Array( 'id'=>$line['id'], 'lng'=>$line['lng'], 'lat'=>$line['lat'], 'zoom'=>$line['zoom'], 'title'=>$line['title'.$langadd], 'shorttext'=>$line['shorttext'.$langadd], );
            $a['url'] = GetSeoUrl('filials',$line['id'],$line);
            
            $filialsarr['items'][] = $a;
            
            $slng += $line['lng'];
            $slat += $line['lat'];
            
            if($id==0 && $k!=0)
            {
                $filialsarr['mainitem']['zoom'] = $line['zoom'];
            }
            
        }
        if($id==0 && $k!=0)
        {
            $filialsarr['mainitem']['lng'] = $slng/$k;
            $filialsarr['mainitem']['lat'] = $slat/$k;
        }
    }
    
    
    //определим текущую страну и текущий город
    //мы можем находится на главной странице филиалов, на странице конкретной страны, на странице города, на странице филиала
    
    //по умолчанию текущая страна и текущий город не определены
    $currcountry = 0; $currcity = 0;

    if($id!=0)
    {
        $sql = "SELECT * FROM $par->filialstable WHERE id=$id";
        //echo $sql.'<BR>';
        $res = mysql_query($sql);
        if($line = mysql_fetch_array($res,MYSQL_ASSOC))
        {
            $filialsarr['mainitem']['id'] = $line['id'];
            $filialsarr['mainitem']['lng'] = $line['lng'];
            $filialsarr['mainitem']['lat'] = $line['lat'];
            $filialsarr['mainitem']['zoom'] = $line['zoom'];
            $filialsarr['mainitem']['title'] = $line['title'.$langadd];
            
            //если у пункта нет родительского элемента - значит мы на страничке страны
            if($line['parentid']==0) {
                $currcountry = $id; $currcity = 0;
            }
            else {
                //иначе нужно определить или мы на странице города или филиала
                
                //найдем родительский пункт
                $sql2 = "SELECT * FROM $par->filialstable WHERE id=".$line['parentid'];
                //echo $sql.'<BR>';
                $res2 = mysql_query($sql2);
                if($line2 = mysql_fetch_array($res2,MYSQL_ASSOC))
                {
                    //если у родительского пункта нет родителя - значит мы на странице города
                    if($line2['parentid']==0){
                        $currcountry = $line['parentid']; $currcity = $id;
                    }
                    else {
                        //а если есть, значит мы на странице филиала
                        $currcountry = $line2['parentid']; $currcity = $line2['id'];
                    
                    }
                }
                
            }
        }
    }
    
    
    $sql = "SELECT * FROM $par->filialstable WHERE `id`=$currcountry";
    $res = mysql_query($sql);
    if($line = mysql_fetch_array($res,MYSQL_ASSOC))
    {
        if($line['parentid']!=0)
        {
            $currcity = $currcountry;
            $currcountry = $line['parentid'];
        }
    }
    
    $filialsarr['countrylist'] = Array();
    $sql = "SELECT * FROM $par->filialstable WHERE `parentid`=0 AND `hide`=0 ORDER BY prior";
    $res = mysql_query($sql);
    while($line = mysql_fetch_array($res,MYSQL_ASSOC))
    {
        $line = LangProcess($line);
        $filialsarr['countrylist'][] = Array('id'=>$line['id'], 'title'=>$line['title']);
    }
    
    $filialsarr['citylist'] = Array();
    $sql = "SELECT * FROM $par->filialstable WHERE `parentid`=$currcountry AND `hide`=0 ORDER BY prior";
    $res = mysql_query($sql);
    while($line = mysql_fetch_array($res,MYSQL_ASSOC))
    {
        $line = LangProcess($line);
        $filialsarr['citylist'][] = Array('id'=>$line['id'], 'title'=>$line['title']);
    }
    
    $filialsarr['currcountry'] = $currcountry;
    $filialsarr['currcity'] = $currcity;

    $_logic['filials'] = $filialsarr;

    //Debug($_logic['filials']);
?>