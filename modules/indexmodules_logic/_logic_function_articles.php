<?

    //////////////ARTICLES FUNCTION
    function GetArticlesItem($id,$line=null,$params=null)
    {
        global $par,$varsline;
        if($line==null)
        {
            $sql="SELECT * FROM $par->news1table WHERE `id`=$id AND `hide`=0";
            $res = mysql_query($sql);
            $line = mysql_fetch_array($res,MYSQL_ASSOC);
        }
        $line = LangProcess($line);
        
        if(isset($line['titleh1']) && $line['titleh1']!='') $line['title']=$line['titleh1']; else $line['title']=$line['title'];
        $line['item_url'] = GetSeoUrl('articles',$line['id'],$line);

        //формируем информацию о фото
        $line['item_fotos'] = Array();
        if(is_file($par->document_root.'/fotos/news1_sm_'.$line['id'].'.jpg'))
        {
                $fname = 'fotos/news1_sm_'.$line['id'].'.jpg';
        }
        else $fname = '';

        if(is_file($par->document_root.'/fotos/news1_bg_'.$line['id'].'.jpg'))
        {
                $fnamebig = 'fotos/news1_bg_'.$line['id'].'.jpg';
        }
        else $fnamebig='';
        $line['item_fotos'][] = Array('fname'=>$fname, 'fnamebig'=>$fnamebig);
        
        return $line;
    
    }
    
    function GetArticlesList($params=null)
    {
        global $par, $varsline;

        $list = Array();
        $sql="SELECT * FROM $par->news1table WHERE `hide`=0";
        if(isset($params['parentid'])) $sql.=" AND `parentid`=".$params['parentid'];
        if(isset($params['orderby'])) $sql.=" ORDER BY ".$params['orderby'];
        if(isset($params['start']) && isset($params['itemsinpage'])) $sql.=" LIMIT ".$params['start']." , ".$params['itemsinpage'];
        
        $res = mysql_query($sql);
        while( $line = mysql_fetch_array($res,MYSQL_ASSOC) )
        {
            $list[] = GetArticlesItem($line['id'],$line);
        }
        return $list;
    }
?>