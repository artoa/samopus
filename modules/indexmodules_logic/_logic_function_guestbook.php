<?

//////////////Guestbook FUNCTION
function GetGuestbookItem($id,$line=null,$params=null)
{
    global $par,$varsline;
    if($line==null)
    {
        $sql="SELECT * FROM $par->guestbooktable WHERE `id`=$id AND `hide`=0";
        $res = mysql_query($sql);
        $line = mysql_fetch_array($res,MYSQL_ASSOC);
    }
    $line = LangProcess($line);

    if(isset($line['titleh1']) && $line['titleh1']!='') $line['title']=$line['titleh1']; else $line['title']=$line['title'];
    $line['item_url'] = GetSeoUrl('guestbook',$line['id'],$line);

    //формируем информацию о фото
    $line['item_fotos'] = Array();
    if(is_file($par->document_root.'/fotos/guestbook_sm_'.$line['id'].'.jpg'))
    {
        $fname = 'fotos/guestbook_sm_'.$line['id'].'.jpg';
    }
    else $fname = '';

    if(is_file($par->document_root.'/fotos/guestbook_bg_'.$line['id'].'.jpg'))
    {
        $fnamebig = 'fotos/guestbook_bg_'.$line['id'].'.jpg';
    }
    else $fnamebig='';
    $line['item_fotos'][] = Array('fname'=>$fname, 'fnamebig'=>$fnamebig);

    return $line;

}

function GetGuestbookList($params=null)
{
    global $par, $varsline;

    $list = Array();
    $sql="SELECT * FROM $par->guestbooktable WHERE `hide`=0 AND `orderstatus`=4";
    if(isset($params['parentid'])) $sql.=" AND `parentid`=".$params['parentid'];
    if(isset($params['orderby'])) $sql.=" ORDER BY ".$params['orderby'];
    if(isset($params['start']) && isset($params['itemsinpage'])) $sql.=" LIMIT ".$params['start']." , ".$params['itemsinpage'];

    $res = mysql_query($sql);
    while( $line = mysql_fetch_array($res,MYSQL_ASSOC) )
    {
        $list[] = GetGuestbookItem($line['id'],$line);
    }
    return $list;
}
?>