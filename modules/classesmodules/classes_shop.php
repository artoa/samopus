<?
    function GetProductInfo($id,$line=null,$paramfunc = null)
    {
            global $par,$langadd,$urllangadd;
            
            $productitem = Array();

            if($line==null)
            {
                    $sql = "SELECT * FROM $par->objectstable WHERE id=$id";
                    $res = mysql_query($sql);
                    $line = mysql_fetch_array($res,MYSQL_ASSOC);
            }
    
    
            if($line==null)
            {
                    $productitem['isdeleted'] = true;
                    return $productitem;
            }
            
            if(isset($line['klonid']) && $line['klonid']!=0)
            {
                    $sql = "SELECT * FROM $par->objectstable WHERE id=".$line['klonid'];
                    $res = mysql_query($sql);
                    $line = mysql_fetch_array($res,MYSQL_ASSOC);
            }

            $myurl = GetSeoUrl('tovar',$line['id'],$line);
            //echo $myurl.'<BR>';

/*		
            //определяем заглавное фото товара
            $fname = 'utils/images_z/nofoto.png';
            $sqltmp = "SELECT * FROM $par->fotorobjtable WHERE reportid=".$line['id']." AND hide=0 ORDER BY prior LIMIT 0,1";
            $restmp = mysql_query($sqltmp);
            if($linetmp = mysql_fetch_array($restmp,MYSQL_ASSOC))
            {
                    if(is_file('fotos/object_sm'.$linetmp['id'].'.jpg'))
                    {
                            $fname = 'fotos/object_sm'.$linetmp['id'].'.jpg';
                    }
            }
            
            $addstr = '';
            if($fname!='')
            {
                    $addstr = GetAddStr(200,150,$fname);
                    $fname = '/'.$fname;
            }
*/		
            //формируем массив фотографий товара
            $objfotos = Array(); $objc = 0;

            $sqltmp = "SELECT * FROM $par->fotorobjtable WHERE reportid=".$line['id']." AND hide=0 ORDER BY prior";
            
            //если не надо все фотографии
            if(! (isset($paramfunc['allfotos']) && $paramfunc['allfotos']==true))
            {
                            $sqltmp.=" LIMIT 0,1";
            }
            $restmp = mysql_query($sqltmp);
            while($linetmp = mysql_fetch_array($restmp,MYSQL_ASSOC))
            {
                    if(is_file($par->document_root.'/fotos/object_sm_'.$linetmp['id'].'.jpg'))
                    {
                            $fname = 'fotos/object_sm_'.$linetmp['id'].'.jpg';
                    }
                    else $fname = '';

                    if(is_file($par->document_root.'/fotos/object_bg_'.$linetmp['id'].'.jpg'))
                    {
                            $fnamebig = 'fotos/object_bg_'.$linetmp['id'].'.jpg';
                    }
                    else $fnamebig='';
                    $objfotos[$objc++] = Array('fname'=>$fname, 'fnamebig'=>$fnamebig);
            }
/*
            $addstr = '';
            if($objc>0 && $objfotos[0]['fname']!='')
            {
                    $addstr = GetAddStr(180,200,$objfotos[0]['fname']);
                    $fname = '/'.$objfotos[0]['fname'];
                    $fnamebig = '/'.$objfotos[0]['fnamebig'];
            }
            else
            {
                            $fname = 'utils/images_z/nofoto.png';
                            $fnamebig = '';
                            $addstr = GetAddStr(180,200,$fname);
                            $fname = '/'.$fname;
            }
*/

            
            $productitem['url'] = $myurl;
            $productitem['fname'] = $fname; //адрес миниатюры товара
            $productitem['fnamebig'] = $fnamebig; //адрес фотки товара
            //$productitem['addstr'] = $addstr; //параметры размеров		// width="xxx" height="xxx"
            $productitem['objfotos'] = $objfotos; //массив всех фото товара
            $productitem['title'] = $line['title'.$langadd];

            if(isset($line['shorttext'.$langadd])) $productitem['shorttext'] = $line['shorttext'.$langadd];
            if(isset($line['text'.$langadd])) $productitem['text'] = $line['text'.$langadd];
            $productitem['image_alt'] = $productitem['title'];
            $productitem['price'] = $line['price'];
            $productitem['pricestr'] = PriceToStr($line['price']);
            if(isset($line['oldprice']))
            {
                    $productitem['oldprice'] = $line['oldprice'];
                    $productitem['oldpricestr'] = PriceToStr($line['oldprice']);
            }
            $productitem['price_valuta'] = 'руб';

            $productitem['id'] = $line['id'];
            $productitem['artikul'] = $line['artikul'];
            $productitem['line'] = $line;
            
            return $productitem;
    }
?>