<?
	function mailer($from, $to, $subj, $body, $paramarr = null)
	{
		global $varsline;
		//если в настройках указана отправка через SMTp сервер
		if(isset($varsline['use_smtp']) && $varsline['use_smtp']==1)
		{
			//SimpleMailer($from,$to,$subj,str_replace("\n","<br>",$body));
			SimpleMailer($from,$to,$subj,$body);
		}
		else
		{
			$from="From: $from\nReply-To: $from\nContent-Type: text/plain; charset=\"utf-8\"\n";
			mail($to,$subj,$body,$from);
		}
		return;
	}
	
	function SimpleMailer($from,$to,$subj,$body,$paramarr = null)
	{
		global $par,$varsline;
		
		if($paramarr==null || $paramarr['smtp_host']=='')
		{
			$paramarr = Array();
			$paramarr['smtp_host'] = $varsline['smtp_host'];
			$paramarr['smtp_port'] = $varsline['smtp_port'];
			$paramarr['smtp_ssl'] = $varsline['smtp_ssl'];
			$paramarr['smtp_username'] = $varsline['smtp_username'];
			$paramarr['smtp_userpass'] = $varsline['smtp_userpass'];
			$paramarr['smtp_title'] = $varsline['smtp_title'];
		}

		if(!isset($paramarr['charset']))
		{
			//если не задана кодировка, то смотрим кодировку сайта, либо utf-8
			if(isset($par->charset)) $paramarr['charset'] = $par->charset;
			else $paramarr['charset'] = 'utf-8';
		}
		if(!isset($paramarr['contenttype'])) $paramarr['contenttype'] = 'text/plain';
		

		require_once($par->document_root.'/utils/phpmailer/class.phpmailer.php');
		
		$mail = new PHPMailer();
		
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
		//$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
							   // 1 = errors and messages
							   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		
		if(isset($paramarr['smtp_ssl']) && $paramarr['smtp_ssl']==1)
		{
			$mail->SMTPSecure = 'ssl';
		}
		
		$mail->Host       = $paramarr['smtp_host']; //"smtp.yandex.ru"; //$par->smtphost;  // sets the SMTP server
		$mail->Port       = $paramarr['smtp_port'];                    // set the SMTP port for the GMAIL server
		$mail->Username   = $paramarr['smtp_username']; // SMTP account username
		$mail->Password   = $paramarr['smtp_userpass'];        // SMTP account password
		
		
		$mail->CharSet    = $paramarr['charset'];
		$mail->ContentType = $paramarr['contenttype'];
  
		//print_r($mail);
		
		$mail->SetFrom($paramarr['smtp_username'], $paramarr['smtp_title']);
		
		$mail->AddReplyTo($paramarr['smtp_username'],$paramarr['smtp_title']);
		
		$mail->Subject    = $subj;
		
		$mail->AltBody    = $body;

		//$mail->MsgHTML($body);
		$mail->MsgHTML($body);
		
		//$address = $to;
		$addressarr = explode(',',$to);
		for($i=0;$i<count($addressarr);$i++)
		{
			$address = trim($addressarr[$i]);
			if($address!='')
			{
				$mail->ClearAddresses();  
				$mail->AddAddress($address, $paramarr['smtp_title']);
		      
				if(!$mail->Send())
				{
				      
				}
			}
		}
		  
		//echo "Mailer Error: " . $mail->ErrorInfo;
		
		//echo date("H:i:s",time()).'<BR>';
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
?>