<?
$content = $_logic['content'];
if($id==0 || count($content['items'])!=0):?>
    <div class="articles box">
    <h1 class="articles-heading mainheading"><?= htmlspecialchars($content['title']); ?></h1>
    <?= $content['text']; ?>
    <? foreach($content['items'] AS $key=>$item):?>

        <div class="article">
            <h2 class="article-title subheading"><a href="<?= $item['item_url']; ?>"><?= htmlspecialchars($item['title']); ?></a></h2>
            <div class="article-date">12.01.2015</div>
            <? if(isset($item['item_fotos'][0]['fname']) && trim($item['item_fotos'][0]['fname'])!=''):?>
                <?  $fname = $item['item_fotos'][0]['fname'];
                $addstr = GetAddStr(200,0,$fname);
                ?>
                <a href="<?= $item['item_url']; ?>" class="article-image"><img src="/<?= $fname; ?>" alt=""/><span class="vfix"></span></a>
            <? endif; ?>
            <div class="article-text ctext">
                <?= $item['shorttext']; ?>
            </div>
            <div class="article-details"><a href="<?= $item['item_url']; ?>">Читати повністю</a></div>
        </div>

    <? endforeach; ?>
    <?
    $pagerarr = $content['pagerarr'];
    include "modules/indexmodules/pager.php";
else:?>

    <div class="articleInner box">
        <h1 class="articleInner-heading mainheading"><?= htmlspecialchars($content['title']); ?></h1>

        <a href="<?= $content['allarticlesurl']; ?>" class="linkBack">ПОВЕРНУТИСЯ ДО ВІДГУКІВ</a>

        <div class="articleInner-content ctext">
            <?= $content['text']; ?>
        </div>
    </div>

<?
endif;

?>