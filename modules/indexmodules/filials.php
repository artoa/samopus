<?
    $filials = $_logic['filials'];
?>
<h1><?= $filials['title']; ?></h1>
<?= $filials['text']; ?>

<!-- подключаем АПИ гугл карт -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script>
    
        var map;
        var initlat = <?= $filials['mainitem']['lat']; ?>;
        var initlng = <?= $filials['mainitem']['lng']; ?>;
        var initzoom = <?= $filials['mainitem']['zoom']; ?>;
        var markers = [];
        var numpoints = 0;
        var mapicon = '/fotos/favicon.jpg';
        
        //функиця добавления маркера на карту
        function addmarker(k, params)
        {
            var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(params.lat, params.lng),
                    map: map,
                    draggable:false,
                    title: params.title,
                    icon: mapicon,
                });
            
            var contentstring = '<h1>'+title+'</h1>'+shorttext;
            var infowindow = new google.maps.InfoWindow({
                  content: contentstring
              });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });
            
                
            
            return marker;
        }
        
        //добавление маркеров и з объекта items
        function GMAP_PutMarkers(items)
        {
            for(var i in items)
            {
                lng = items[i].lng;
                lat = items[i].lat;
                zoom = items[i].zoom;
                title = items[i].title;
                shorttext = items[i].shorttext;
                markers[numpoints] = addmarker(numpoints, {'lng':lng, 'lat':lat, 'zoom':zoom, 'title':title, 'shorttext':shorttext, });

                numpoints++;
            }
        }
        
        //удаление всех маркеров с карты
        function GMAP_RemoveMarkers()
        {
            if (markers) {
              for (i in markers) {
                if (markers[i]!=null) {
                    markers[i].setMap(null);
                    markers[i] = null;
                }
              }
            }                 
        }
        
        //подгрузка AJAX'ом нужных маркеров на карту
        function LoadMarkers(id)
        {
            if (id==0) return;

            rand1 = Math.random();

            $.ajax({
                dataType: "json",                
                url: '/forajax.php?act=loadmarkers&id='+id+'&rand1='+rand1, 
                success: function (data, textStatus) {

                    //убираем старые маркеры
                    GMAP_RemoveMarkers();
                    
                    //ставим новые маркеры
                    GMAP_PutMarkers(data.items);
                    
                    //устанавливаем на карте новый центр и масштаб
                    map.setZoom(data.mainitem.zoom*1);
                    map.setCenter(new google.maps.LatLng(data.mainitem.lat*1.0, data.mainitem.lng*1.0));
                    
                }
            });	
                
        }
        
        //подгрузка AJAX'ом нужных городов во второй селект
        function LoadRegions(id)
        {
            rand1 = Math.random();
    
            $.ajax({
                url: '/forajax.php?act=loadmapregions&id='+id+'&rand1='+rand1, 
                success: function (data, textStatus) {
                    $('#mapregion2').html(data);
                }
            });	
                
        }
        
        
        $(document).ready(function()
        {
            var myLatlng = new google.maps.LatLng(initlat, initlng);

            var myOptions = {
                zoom: initzoom,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            
            map = new google.maps.Map(document.getElementById("Gmap1"), myOptions);
            
            <?
                if(count($_logic['filials']['items'])>0) $m = json_encode($_logic['filials']['items']);
                else $m = json_encode( Array($_logic['filials']['mainitem']));
            ?>
            
            var items = eval(<?= $m; ?>);
            GMAP_PutMarkers(items);

        });
        
</script>


Страна:
<select onchange="LoadMarkers(this.value); LoadRegions(this.value)">
    <option value="0">---------</option>
<?
    foreach($filials['countrylist'] AS $key=>$item)
    {
        ?> <option value="<?= $item['id']; ?>" <? if($item['id']==$filials['currcountry']) echo ' selected '; ?>><a href="<?= $item['url']; ?>"><?= htmlspecialchars($item['title']); ?></a></option> <?
    }
?>
</select>

Город:
<select onchange="LoadMarkers(this.value);" id="mapregion2">
    <option value="0">---Выберите город---</option>
<?
    foreach($filials['citylist'] AS $key=>$item)
    {
        ?> <option value="<?= $item['id']; ?>" <? if($item['id']==$filials['currcity']) echo ' selected '; ?>><a href="<?= $item['url']; ?>"><?= htmlspecialchars($item['title']); ?></a></option> <?
    }
?>
</select>


<div id="Gmap1" style="width:600px; height:400px; border:1px solid #000; margin-bottom:5px;"></div>

<?
    foreach($filials['items'] AS $key=>$item)
    {
        ?> <a href="<?= $item['url']; ?>"><?= htmlspecialchars($item['title']); ?></a><br/> <?
    }
?>
