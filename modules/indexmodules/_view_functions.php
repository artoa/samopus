<?
    function PrintProductBlock($product)
    {
        $addstr = GetAddStr(194,150,$product['fname']);
        
        ?>
        <div class="item">
                <div class="item-holder">
                        <div class="img-holder">
                                <span><a href="<?= $product['url']; ?>"><img src="/<?= $product['fname']; ?>" <?= $addstr; ?> alt="<?= htmlspecialchars($product['image_alt']); ?>" /></a></span>
                        </div>
                        <strong class="price"><?= $product['pricestr'].' '.$product['price_valuta']; ?> </strong>
                </div>
                <div class="description">
                        <a href="<?= $product['url']; ?>"><?= htmlspecialchars($product['title']); ?></a>
                        <dl>
                                <dt>Код:</dt>
                                <dd>134999</dd>
                                <dt>На складе:</dt>
                                <dd>есть</dd>
                        </dl>
                </div>
                <a href="#" <?= ' onclick="AddToBasket('.$product['id'].','.$product['price'].'); return false;" '; ?> class="order-now">Купить</a>
                <span class="sale">10%</span>
        </div>
        <?
                
    }
    
    function PrintProductBlocks($products)
    {
        ?>
        <div class="items-block">
        <?
                foreach($products AS $key=>$product)
                {
                    PrintProductBlock($product);
                }
        ?>
        </div>
        <?
    }
?>