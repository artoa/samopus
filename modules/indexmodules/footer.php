            </div>
        </div>
        <div class="footer_placeholder"></div>
        <footer class="footer">
            <div class="mbox">
                <div class="footer-left">
                    <div class="social">
                        <div class="social-heading subheading">СОЦ. МЕРЕЖІ:</div>
                        <div class="social-items rope">
                            <a href="<?=$varsline["link_vk"]?>" class="social-item icon-vk"></a>
                            <a href="<?=$varsline["link_fb"]?>" class="social-item icon-fb"></a>
                            <a href="<?=$varsline["link_google"]?>" class="social-item icon-gg"></a>
                            <a href="<?=$varsline["link_ins"]?>" class="social-item icon-inst"></a>
                        </div>
                    </div>
                </div>

                <div class="footer-right">
                    <nav class="footer-menu">
                        <ul>
                            <?foreach($_logic['mainmenuarr'] AS $item):?>
                                    <li<?if($item["isactive"]){?> class="active"<?}?>><a href="<?=$item["url"]?>"><?=htmlspecialchars($item["title"])?></a></li>
                            <?endforeach;?>
                        </ul>
                    </nav>



                    <div class="copyright"><?=$varsline["copy"]?></div>
                </div>
            </div>
        </footer>



        <div class="counter_code">
            <?=$varsline['countercode']?>
        </div>


            <? if(isset($_SESSION['sent']) && $_SESSION['sent']==1): ?>

                <script>
                    myAlert('<?=$_SESSION['sent_title']?>', '<?=$_SESSION['sent_text']?>', 'ok');
                </script>
                <? $_SESSION['sent']=0; ?>
                <? $_SESSION['sent_text']=''; ?>
                <? $_SESSION['sent_title']=''; ?>
            <? endif; ?>
    </body>
</html>