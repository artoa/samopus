<?if($id==0){?>
<!-- gallery album catalog -->
<div class="gallery box">
    <h1 class="gallery-heading mainheading"><?=htmlspecialchars($_logic['content']["title"])?></h1>

    <?
    $pagerarr = $_logic['content']['pagerarr'];
    include "modules/indexmodules/pager.php";
    ?>
    <div class="gallery-content">
        <?foreach($_logic['gallery_arr'] AS $gallitem):?>
            <a href="<?=$gallitem['url']?>" class="gallery-item">
                <img src="<?=$gallitem["img"]?>" alt="<?=htmlspecialchars($gallitem['title'])?>"/>
                <span class="gallery-item-footer">
                    <span class="gallery-item-desc">
                        <?=htmlspecialchars($gallitem['title'])?>
                    </span>
                </span>
            </a>

            <?endforeach;?>
    </div>
    <?
    $pagerarr = $_logic['content']['pagerarr'];
    include "modules/indexmodules/pager.php";
    ?>
</div>
<?}else{?>

    <?$gallery_arr = $_logic['gallery_arr'];?>
    <div class="galleryAlbum box">
        <h1 class="galleryAlbum-heading mainheading"><?=htmlspecialchars($gallery_arr['title'])?></h1>
        <a href="<?=$gallery_arr['backurl']?>" class="linkBack">ПОВЕРНУТИСЯ ДО ГАЛЕРЕЇ</a>

        <!-- auto image grid.
            add class
                h2 - to set double height;
                w2 - double width;
                w2 h2 - to set double width and height
            image size:
                horizontal : ~440x294px
                vertical : ~294x440px
        -->
        <div class="preloader"></div>
        <div class="galleryAlbum-images zHiddenBlock" >
            <?$i=1; foreach($gallery_arr['fotos'] AS $item):?>
                <a href="<?=$item["fnamebig"]?>" data-fancybox-group="group1" class="fancybox-img galleryAlbum-image
                <?if($i==1||$i==5) echo "w2 h2"; if($i==6||$i==7) echo "h2";?>
                "><img src="<?=$item["fname"]?>" alt="<?=$item["title"]?>" title="<?=$item["title"]?>"/></a>
            <?$i++; if($i==13) $i=1; endforeach;?>
        </div>
    </div>

<?}?>