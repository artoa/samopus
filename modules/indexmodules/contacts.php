<?$pageitem = $_logic['pageitem'];?>

<div class="contacts box">
    <h1 class="contactsHeading mainheading"><?=htmlspecialchars($pageitem["title"])?></h1>
    <div class="contactsCols">
        <div class="contactsCol ctext">
            <h2>Дзвоніть:</h2>

            <p><?=$varsline["contacts_phone"]?></p>

            <h2>Пошта:</h2>
            <p><a href="mailto:<?=$varsline["contacts_email"]?>"><?=$varsline["contacts_email"]?></a></p>

            <h2>Skype:</h2>
            <p><a href="skype:<?=$varsline["contacts_skype"]?>?call"><?=$varsline["contacts_skype"]?></a></p>

            <h2>Ми в соц. мережах:</h2>
            <div class="social-small">
                <div class="social-small-row"><a href="<?=$varsline["link_vk"]?>" class="social-item icon-vk"></a><a href="<?=$varsline["link_vk"]?>"><?=$varsline["link_vk"]?></a></div>
                <div class="social-small-row"><a href="<?=$varsline["link_fb"]?>" class="social-item icon-fb"></a><a href="<?=$varsline["link_fb"]?>"><?=$varsline["link_fb"]?></a></div>
                <div class="social-small-row"><a href="<?=$varsline["link_google"]?>" class="social-item icon-gg"></a><a href="<?=$varsline["link_google"]?>"><?=$varsline["link_google"]?></a></div>
                <div class="social-small-row"><a href="<?=$varsline["link_ins"]?>" class="social-item icon-inst"></a><a href="<?=$varsline["link_ins"]?>"><?=$varsline["link_ins"]?></a></div>
            </div>
        </div>
        <div class="contactsCol">
            <form action="" class="zNice contactsForm">
                <h2 class="form-heading subheading">Пишіть нам:</h2>
                <div class="form-row">
                    <div class="form-label"><span class="label-title">Ім'я:</span><span class="vfix"></span></div>
                    <div class="form-input"><input type="text" required name="uname" data-image="/images/icon-human.png" /></div>
                </div>
                <div class="form-row">
                    <div class="form-label"><span class="label-title">Номер телефону:</span><span class="vfix"></span></div>
                    <div class="form-input"><input type="text" required phone name="uphone" data-image="/images/icon-phone.png" /></div>
                </div>
                <div class="form-row">
                    <div class="form-label"><span class="label-title">Email:</span><span class="vfix"></span></div>
                    <div class="form-input"><input type="email" required  name="uemail" data-image="/images/icon-mail.png" /></div>
                </div>
                <div class="form-row">
                    <div class="form-label"><span class="label-title">Текст повідомлення:</span><span class="vfix"></span></div>
                    <div class="form-input"><textarea required name="contact_mess" rows="7"></textarea></div>
                </div>
                <div class="form-submit">
                    <input type="submit" value="ВІДПРАВИТИ ЗАЯВКУ"/>
                </div>
            </form>
        </div>
    </div>
</div>
