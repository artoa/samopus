<? $k=0; ?>
<div>
    
<ul class="breadcrumbs">
    <li><a href="<?= GetSeoUrl('none'); ?>"><img src="/images/home.png" style="height: 24px;"></a></li>
    <? foreach( array_reverse($_breadcrumbsarr,true) AS $key=>$item): ?>
        <? $k++; ?>      
        <? if($k==count($_breadcrumbsarr)): ?>
            <li><?= htmlspecialchars($item['item_title']); ?></li>
        <? else: ?>
            <li><a href="<?= htmlspecialchars($item['item_url']); ?>"><?= htmlspecialchars($item['item_title']); ?></a></li>
        <? endif; ?>
    <? endforeach; ?>
</ul>
</div>
