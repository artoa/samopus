<?
    $pageitem = $_logic['pageitem'];
?>

<!-- Content -->
<div id="content">
    <div id="blog" class="container section">
        <div id="blog-content" class="<?if($pageitem['spec']==1){?>two-third<?}?> column end">
            <div id="post-content" class="blog-item ctext">
                <h1><?=htmlspecialchars($pageitem['title'])?></h1>
                <?=$pageitem['text']?>
            </div>
        </div>
        <?if($pageitem['spec']==1){?>
            <div id="sidebar-content" class="one-third column last">
                <div class="sidebar-item">

                    <?include "modules/indexmodules/callform.php";?>

                    <div class="banImg">
                        <?=$pageitem['banner'];?>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
</div>