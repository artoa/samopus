<? //если страница списка новостей ?>
<? if($act=="news" && $id==0){?>

    <div class="main">
        <section class="content">
            <div class="mbox"><h2 class="subheading"><?= htmlspecialchars($_logic['content']['title']); ?></h2></div>
            <div class="newsItems">

                <?
                $total = count($_logic['content']['items']);
                $counter = 0;
                foreach($_logic['content']['items'] AS $key=>$item):?>

                    <article class="newsItem mbox">

                            <? if(isset($item['item_fotos'][0]['fname'])):?>
                                <?  $fname = $item['item_fotos'][0]['fname'];?>
                                <div class="newsItem-left">
                                <div class="newsItem-img"><img class="img-thumb" src="/<?=$fname; ?>" alt="<?= htmlspecialchars($item['title']); ?>"/></div>
                        </div>
                            <? endif; ?>


                        <div class="newsItem-right">
                            <div class="newsItem-title"><a href="<?=$item['item_url']?>"><?=htmlspecialchars($item['title'])?></a></div>
                            <div class="newsItem-details"><span><?=MyDate($item['date'])?></span></div>
                            <div class="newsItem-intro">
                               <?=$item['shorttext']?>
                            </div>
                            <a href="<?=$item['item_url']?>" class="btn-yellow btn-readmore">Подробнее<span class="icon icon-rarr"></span></a>
                        </div>
                    </article>
                    <?
                        $counter++;
                        if ($counter != $total){
                    ?>
                        <div class="sep"></div>
                    <?}?>

                <?endforeach;?>

                <div class="clear"></div>

                <?
                $pagerarr = $_logic['content']['pagerarr'];
                include "modules/indexmodules/pager.php";
                ?>

            </div>

        </section>
        <div class="clear"></div>
    </div>

<?}else{ ?>

    <? $item = $_logic['content']; ?>

    <div class="main">
        <div class="mbox">
            <section class="content">
                <? if(isset($item['item_fotos'][0]['fname'])):?>
                    <?  $fname = $item['item_fotos'][0]['fname'];?>
                    <div class="newsItem-left">
                        <div class="newsItem-img"><img class="img-thumb" src="/<?=$fname; ?>" alt="<?= htmlspecialchars($item['title']); ?>"/></div>
                    </div>
                <? endif; ?>

                <div class="ctext">
                    <div class="ctext-title"><h2><?=$item['title']?></h2></div>
                    <?=$item['text']?>
                </div>
            </section>
            <a href="<?= $item['allnewsurl']; ?>" style="float:right;">Все новости</a>

            <div class="clear"></div>
        </div>
    </div>



<?}?>

