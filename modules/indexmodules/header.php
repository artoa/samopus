<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?
	$titletitle = $_logic['headerarr']['titletitle'];
	$titlekeywords = $_logic['headerarr']['titlekeywords'];
	?>
	<title><?= htmlspecialchars($titletitle); ?></title>
	<?= $titlekeywords; ?>

    <meta charset="utf-8">
    <!--[if ie]><meta content='IE=8' http-equiv='X-UA-Compatible'/><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <? if(isset($_logic['headerarr']['favicon'])): ?>
		<link rel="icon" href="<?= $_logic['headerarr']['favicon']; ?>" type="image/x-icon" />
		<link rel="shortcut icon" href="<?= $_logic['headerarr']['favicon']; ?>" type="image/x-icon" />
	<? endif; ?>

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="/css/reset.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/css/style.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/css/jquery.znice.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/fancybox/jquery.fancybox.css" media="all" />

    <script src="/js/jquery-1.10.0.min.js" type="text/javascript"></script>


    <!--[if lt IE 9]>
    <script>
        document.createElement('header');
        document.createElement('nav');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
        document.createElement('time');
    </script>
    <script src="/js/pie.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if lt IE 8]><script src="/js/oldie/warning.js"></script><script>window.onload=function(){e("js/oldie/")}</script><![endif]-->
</head>


<body>
<header class="header">
    <div class="headerPlaceholder"></div>
    <div class="header-top">
        <div class="mbox">
            <nav class="header-menu">
                <ul>
                    <?foreach($_logic['mainmenuarr'] AS $item):?>
                        <?if(!$item["spec"]){?>
                            <li<?if($item["isactive"]){?> class="active"<?}?>><a href="<?=$item["url"]?>"><?=htmlspecialchars($item["title"])?></a></li>
                        <?}?>
                    <?endforeach;?>
                </ul>
            </nav>

            <div class="header-button">
                <a href="#order" class="button fancybox">ЗАЯВКА НА ПОДОРОЖ</a>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="mbox">


            <div class="header-phone">
                <p class="phone-title">Завжди на зв'язку:</p>
                <p><a href="tel:+38012345678" class="phone-number"><span class="phone-code">+38 (012)</span>345-67-89</a></p>
            </div>
<?//debug($_logic)?>
            <div class="lang box rope">
                <div class="lang-heading" title="Language select/выбрать язык">Обрати мову:</div>

            </div>

            <div class="header-social box">
                <div class="social">
                    <a href="<?=$varsline["link_vk"]?>" class="social-item icon-vk"></a>
                    <a href="<?=$varsline["link_fb"]?>" class="social-item icon-fb"></a>
                    <a href="<?=$varsline["link_google"]?>" class="social-item icon-gg"></a>
                    <a href="<?=$varsline["link_ins"]?>" class="social-item icon-inst"></a>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="main">
    <div class="mbox">