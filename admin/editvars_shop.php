<?
	class Vars_ShopAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editvars_shop";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = '

		   file, reviewsinpage,articlesinpage,fotoalbumsinpage,copy,contacts_phone,contacts_email,contacts_skype,link_vk,link_fb,link_google,link_ins,

		    
		';
		var $fields_list_str = '';
		
		var $pagestr = ''; //префикс url для модуля
		var $maxlevel = 1;
                var $candelete = false;
		
		var $one_record_module = true; //если true - то модуль будет обсулуживать только 1 запись в базе данных
	
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->varstable;


            $this->fields['file'] = Array('fieldtype'=>'text', 'visualtype'=>'file', 'fileprefix' => 'files/video_',/*Префикс пути для загружаемого файла*/   'fieldhint'=>'Файл1', 'multilang'=>false,  'tdname'=>'Файл1');

            $this->fields['map2'] = Array('fieldtype'=>'none', 'visualtype'=>'map',   /*настройки карты*/ 'paramarr'=>Array( 'mapprovider'=>'google', 'default_lng'=>37.627038415521405, 'default_lat'=>55.745429334465435 , 'default_zoom'=>5 , /*По умолчанию координаты Москвы*/  'map_width'=>600, 'map_height'=>400,  'prefix_lng'=>'lng',  'prefix_lat'=>'lat',  'prefix_zoom'=>'zoom', /*префикс в названии полей в таблице у lat, lng, zoom*/  ),   /*конец настроек карты*/   'fieldhint'=>'Карта на странице "Контакты"', 'multilang'=>false,  'sortable'=>false,/*Если не нужна сортировка по этому полю в списке*/ 'tdname'=>'Карта', 'listeditable'=>false, );
            $this->fields['speed_slider'] = Array('fieldtype'=>'int', 'visualtype'=>'input', 'fieldhint'=>'Скорость смены слайдера (0 - запрет автопереключения)', 'multilang'=>false,  'sortable'=>false,/*Если не нужна сортировка по этому полю в списке*/ 'tdname'=>'Описание');
            $this->fields['shorttext'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Текст над значком карты', 'multilang'=>true,  'sortable'=>false,/*Если не нужна сортировка по этому полю в списке*/ 'tdname'=>'Описание');
            $this->fields['title'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Заголовок значка карты', 'multilang'=>true,  'sortable'=>false,/*Если не нужна сортировка по этому полю в списке*/ 'tdname'=>'Описание');

            $this->fields['favicon2'] = Array('fieldtype'=>'text', 'visualtype'=>'image', 'fieldhint'=>'Иконка значка на карте', 'multilang'=>false,  'tdname'=>'Fav',
                'pics' => Array(
                    'params' => Array(
                        Array('picprefix'=>'fotos/favicon2', 'w'=>32, 'h'=>32, 'mode'=>'same','ext'=>'jpg', 'idmode'=>'withoutid'),
                    )
                ),
            );

           

            $this->fields['address'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Адрес', 'multilang'=>true,  );
            $this->fields['contacts_phone'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Телефоны в контактах', 'multilang'=>true,  );
            $this->fields['contacts_email'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Email в контактах', 'multilang'=>true,  );
            $this->fields['contacts_skype'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Skype в контактах', 'multilang'=>true,  );
            $this->fields['hlang'] = Array('fieldtype'=>'int', 'visualtype'=>'checkbox', 'fieldhint'=>'Вывести переключатель языков?', 'multilang'=>false,  );

            $this->fields['banner_sidebar_def'] = Array('fieldtype'=>'text', 'visualtype'=>'fck', 'fieldhint'=>'Стандарнтый баннер справа', 'multilang'=>false,  );

            $this->fields['meteo_plagin'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Плагин погоды', 'multilang'=>false,  );
            $this->fields['link_vk'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Ссылка на vkontakte', 'multilang'=>false,  );
            $this->fields['link_fb'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Ссылка на facebook', 'multilang'=>false,  );
            $this->fields['link_google'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Ссылка на google+', 'multilang'=>false,  );
            $this->fields['link_ins'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Ссылка на instagram', 'multilang'=>false,  );


            $this->fields['logo'] = Array('fieldtype'=>'text', 'visualtype'=>'image', 'fieldhint'=>'Логотип', 'multilang'=>false,  'tdname'=>'Fav' , 'fieldhelp'=>'',
                'pics' => Array(
                    'params' => Array(
                        Array('picprefix'=>'images/logo', 'w'=>32, 'h'=>32, 'mode'=>'same','ext'=>'png', 'idmode'=>'withoutid'),
                    )
                ),
            );





			$this->fields['articlesinpage'] = Array('fieldtype'=>'int', 'visualtype'=>'input', 'fieldhint'=>'Статей на страницу', 'multilang'=>false,  );

			$this->fields['reviewsinpage'] = Array('fieldtype'=>'int', 'visualtype'=>'input', 'fieldhint'=>'Отзывов на страницу', 'multilang'=>false,  );
			$this->fields['travelinmain'] = Array('fieldtype'=>'int', 'visualtype'=>'input', 'fieldhint'=>'Путешествий на главной', 'multilang'=>false,  );
			$this->fields['travelinpage'] = Array('fieldtype'=>'int', 'visualtype'=>'input', 'fieldhint'=>'Путешествий на страницу', 'multilang'=>false,  );


			$this->fields['fotoalbumsinpage'] = Array('fieldtype'=>'int', 'visualtype'=>'input', 'fieldhint'=>'Фотоальбомов на страницу', 'multilang'=>false,  );
			$this->fields['fotosinpage'] = Array('fieldtype'=>'int', 'visualtype'=>'input', 'fieldhint'=>'Фотографий на страницу', 'multilang'=>false,  );
			$this->fields['copy'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Копирайт на сайте', 'multilang'=>true,  );

			

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>