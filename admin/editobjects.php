<?
	class ObjectsAdminModule Extends BasicAdminModule
	{
		var $moduletype = Array(
					Array('moduletype'=>'objectstype', 'cattablename'=>'', 'catmoduleact'=>'editcat', 'catmodulefile'=>'editcat.php', 'catclassname'=>'CatAdminModule',),
				    );
		
		var $moduleact = "editobjects";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе

		var $fields_str = 'id, artikul, categid, title, titleh1, seo, spec1, spec2, spec3, price, priceold, gallery1, text, titletitle, titlekeywords, titledescription ';
		var $fields_list_str = 'id, artikul, gallery1, title, price';
		
		var $pagestr = 'tovar'; //префикс url для модуля
		var $maxlevel = 1;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->objectstable;
			$this->moduletype[0]['categorytable'] = $par->categorytable;
			
			$this->fields['title']['filter'] = Array('filtertype'=>'text', 'filterwidth'=>200, );

			$this->hints['maincategories'] = 'товары';
			
			$this->fields['gallery1'] = Array('fieldtype'=>'text', 'visualtype'=>'gallery',  'fieldhint'=>'Фото', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStandart', 'tdname'=>'Фото', 'list_td_width'=>100,
					'gallerypics_tablename' => 'fotorobj',
					'gallerypics' => Array(
								Array('picprefix'=>'fotos/object_sm_', 'w'=>194, 'h'=>150, 'mode'=>'bigsize','ext'=>'jpg'),
								Array('picprefix'=>'fotos/object_bg_', 'w'=>600, 'h'=>400, 'mode'=>'bigsize','ext'=>'jpg',/*'watermarkfile'=>'../images/water.png','watermarkpos'=>'center'*/)
							),	
					);
					
			$this->fields['spec1'] = Array('fieldtype'=>'int', 'visualtype'=>'checkbox', 'fieldhint'=>'Новинка', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStandart', 'tdname'=>'Нов');
			$this->fields['spec2'] = Array('fieldtype'=>'int', 'visualtype'=>'checkbox', 'fieldhint'=>'Хит продаж', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStandart', 'tdname'=>'Хит');
			$this->fields['spec3'] = Array('fieldtype'=>'int', 'visualtype'=>'checkbox', 'fieldhint'=>'Акция', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStandart', 'tdname'=>'Акц');
			
			$this->fields['artikul']['listeditable'] = true;

			$this->fields['price']['listeditable'] = true;
			$this->fields['price']['filter'] = Array('filtertype'=>'int', 'filterwidth'=>50);
					
			$this->fields['categid'] = Array('fieldtype'=>'int',   /*для select*/ 'visualtype'=>'select',  'selecttable'=>'category', 'selecttablefield'=>'title', 'selectorderfield'=>'id ASC', 'selectmaxlevel'=>4,/*end - для select*/          'fieldhint'=>'Категория', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStandart', 'tdname'=>'Категория');
					

			$this->itemsorder = "prior DESC"; //ASC or DESC порядок сортировки пунктов в админке

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>