<?

	class AdminFunc
	{
		var $path = '/admin/drawer/drawer_oc/';

		function DrawLine($v)
		{
			$ret = '';
			$ret.= '<tr valign="top"><td width="200"><b>'.$this->DrawItem($v[0]).'</b></td><td>'.$this->DrawItem($v[1]);
			$ret.= '</td></tr>';
			$ret.= '<tr valign="top"><td colspan="2"><div class="admhr"></div></td></tr>';
			
			return $ret;
			
		}
		
		function DrawItem($v)
		{
			if(count($v)>0)
			{
				if($v['type']=='line')
				{
					return $this->DrawLine($v['value']);
				}

				if($v['type']=='text') return $v['value'];
				if($v['type']=='html') return $v['value'];
				
				if($v['type']=='multiline')
				{
					//echo 'MULTILINE';
					//print_r($v);
					$ret = '';
					foreach($v['value'] AS $vline)
					{
						$ret.=$this->DrawItem($vline);
					}
					return $ret;
				}

				//if($v['type']=='input') $this->DrawInput($v['value']);
			}
		}
		
		function DrawTableLine($v,$v2='',$v3='') //печатает строку таблицы списка элементов
		{
			$ret = '';
			
			if($v=='table_begin')
			{
				if($v2=='noneform')
				{
					$ret = '            
					        <table class="list">
							<tbody>';
				}
				
				if($v2=='editform')
				{
					$ret = 	'
					<table border="0" cellpadding="3" cellspacing="3" width="95%"><tbody>';

				}
			}
			else if($v=='table_end')
			{
				$ret = '</tbody></table>';
			}
			else
			{
				$ret = '';
				
				$ret.= '
				<tr class="'.$v3.'">';
				
				foreach($v AS $key1=>$value1)
				{
					$key = $value1['type']; $value = $value1['value'];
					if(isset($value1['list_td_width'])) $addwstr = ' width:'.$value1['list_td_width'].'px; '; else $addwstr = '';

					if(isset($value1['td_color'])) $addcolstr = ' background-color:'.$value1['td_color'].'; '; else $addcolstr = '';

					if($key=='td')
					{
						
						$ret.= '
						<TD style=" '.$addcolstr.' '.$addwstr.' ">'.$value.'</td>';
					}
					
					if($key=='td_22')
					{
						$ret.= '
						<TD width="22" style="width:22px;">'.$value.'</td>';
					}
				}
				$ret.= '
				</tr>';
				
			}
			return $ret;
		}
		
		
		function DrawTableHeader($v,$v2='',$v3='') //печатает строку таблицы списка элементов
		{
			$ret = '';
			
			$ret.= '
			<tr class="'.$v3.'">
				<form method="post" action="">
				<input type="hidden" name="subact" value="filter">
			';
			
			foreach($v AS $key1=>$value1)
			{
				$key = $value1['type'];
				if(isset($value1['tdname'])) $value = $value1['tdname']; else $value = ' ';
				if(isset($value1['sorturl'])) $sorturl = $value1['sorturl']; else $sorturl = '#';
				if(isset($value1['ascdesc'])) $ascdesc = $value1['ascdesc']; else $ascdesc = '';
				
				$adds = '';
				
				//Debug($value1);
				if(isset($value1['listeditable']) && $value1['listeditable']==true)
				{
					$adds.=' <a href="#" onclick="ChangeFields(); return false;">'.$this->DrawIcon('save',' width="16" ').'</a>';
				}
				
				
				if(isset($value1['field_key']) && isset($value1['filter']))
				{
					if(isset($value1['filter']['filterwidth'])) $filterwidth = $value1['filter']['filterwidth']; else $filterwidth = 50;
					
					if($value1['filter']['filtertype']=='text')
					{
						$adds .= ' <br> <input type="text" name="__filter['.$value1['field_key'].']" style="width:'.$filterwidth.'px;" value="'.htmlspecialchars($value1['filter_value']).'" > ';
					}

					if($value1['filter']['filtertype']=='int')
					{
						if(isset($value1['filter_value'][0])) $fval0 = $value1['filter_value'][0]; else $fval0 = '';
						if(isset($value1['filter_value'][1])) $fval1 = $value1['filter_value'][1]; else $fval1 = '';
						
						$adds .= '
						<br>
						<input type="text" name="__filter['.$value1['field_key'].'][]" style="width:'.$filterwidth.'px;" value="'.htmlspecialchars($fval0).'" ><br>
						до<br>
						<input type="text" name="__filter['.$value1['field_key'].'][]" style="width:'.$filterwidth.'px;" value="'.htmlspecialchars($fval1).'" >
						';
					}
				}

				if(isset($value1['tdcontrol']) && $value1['tdcontrol']=='mascheckbox')
				{
					$ret .='<td><input type="checkbox" onclick=""></td>';
				}
				else if($key=='td')
				{
					if(isset($value1['list_td_width'])) $addwstr = ' width:'.$value1['list_td_width'].'px; ';
					else $addwstr = '';
					
					$ret.= '
					<TD style="font-weight:bold; "><div style="'.$addwstr.'"><a '.($sorturl==''?'':'href="'.$sorturl.'"').'>'.$value.'</a>'.($ascdesc==''?'':$this->DrawIcon($ascdesc)).$adds.'</div></td>';
				}
				else if($key=='td_22')
				{
					$ret.= '
					<TD style="font-weight:bold; width:22px;">'.$value.$adds.'</td>';
				}
			}
			$ret.= '
				<input type="image" value="" src="" style="width:0px; height:0px;">
				</form>
			</tr>';

			return $ret;
		}
		    
        //Начало шапки
        function DrawAdminHeader()
        {
?><!DOCTYPE html>
<html dir="ltr" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Панель управления</title>

<link rel="stylesheet" type="text/css" href="/admin/drawer/drawer_oc/adminfiles/style.css">
	

<script src="plugins/ckeditor/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" href="/admin/drawer/drawer_oc/adminfiles/gallery.css">
<!--
<script src="/admin/adminjs/fancybox/jquery-1.3.2.min.js"></script>
-->

<script src="/admin/adminjs/jquery-1.7.1.min.js" type="text/javascript"></script>

<!-- colorpicker -->
<link rel="Stylesheet" type="text/css" href="/admin/plugins/colorpicker/css/jpicker-1.1.6.min.css" />
<link rel="Stylesheet" type="text/css" href="/admin/plugins/colorpicker/jPicker.css" />
<script src="/admin/plugins/colorpicker/jpicker-1.1.6.min.js" type="text/javascript"></script>

<script>
	var admhints = new Array();
	<? $k = 0; ?>
	<? foreach($GLOBALS['_adm_hints'] AS $key=>$value): ?>
		admhints[<?= $k; ?>]=['<?= $key; ?>','<?= addslashes($value); ?>']
	<? $k++; ?>	
	<? endforeach; ?>

</script>

<script src="/admin/adminjs/scr.js"></script>


<style>
	.list tr.over td {
	    background: #f7f7f7;
	}	
</style>

<script>
	
	function ChangeFields()
	{
		msg = $(".listeditablefield").serialize();
		
		r = Math.random(1,100000);
		$.ajax({
			type: "POST",
			url: "workajax.php?act=changefields&r="+r,
			//data: { 'msg': msg, },
			data: msg,
			success: function(data) {
			  alert('Сохранено');
			},
			error:  function(xhr, str){
			      alert('Возникла ошибка: ' + xhr.responseCode);
			  }			
		});		
	}
	
	function ChangeHide(modulename,tablename,showhide,id,obj)
	{
		src_str = $('#test'+id).children('img').attr('src');
		showhide = (src_str.indexOf('on.gif')> -1) ? 1 : 0;
		r = Math.random(1,100000);
		$.ajax({
			type: "GET",
			url: "workajax.php?act=changehide&module="+modulename+"&tablename="+tablename+"&showhide="+showhide+"&id="+id+"&r="+r,
			//data: { 'msg': msg, },
			success: function(data) {
			  dimg = $('#test'+id).children('img');
			  d = dimg.attr('src');
			  d1 = d.replace('off.gif','on.gif');
			  d2 = d.replace('on.gif','off.gif');
			  if (d2==d) dimg.attr('src',d1);
			  else dimg.attr('src',d2);

			},
			error:  function(xhr, str){
			      alert(' ошибка: ' + xhr.responseCode);
			  }			
		});
	}
	
	$(document).ready(
	  function()
	  {
		$(".list tr").mouseover(function() {
			$(this).addClass("over");
		});
	  
		$(".list tr").mouseout(function() {
			$(this).removeClass("over");
		});
		
		$(".list tr:even").addClass("alt");
	  }
	);

</script>

</head>
<body>

<div id="container">
<div id="header">
  <div class="div1">
    <div class="div2">
	<a href="/admin/admin.php">Панель управления</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="/">На сайт</a>
	
	</div>
	
    <div class="div3">
		
	<?= GetDrawerSelect(); ?>
		
		<img src="/admin/drawer/drawer_oc/adminfiles/lock.png" alt="" style="position: relative; top: 3px;">
    <? echo date("d.m.Y",time()); ?>&nbsp;&nbsp;<BR>
          <? if(isset($_SESSION['loguser']) && $_SESSION['loguser']==1) echo htmlspecialchars($_SESSION['loguserfio']); else echo 'admin'; ?>&nbsp;&nbsp;(<A
      href="admin.php?act=logout"><B>Выход</B></A>)&nbsp;&nbsp;
	
	</div>
  </div>
    <div id="menu">
    <ul class="left sf-js-enabled" style="display: block;">
<?          
        }


        function DrawHeadCategory($act,$temp1_act,$temp1_title,$temp1_file,$temp1_url,$subitems)
        {
		
		$subitems_html = '';
		if(isset($subitems)):
			$subitems_html.='
			<ul class="sub_menu">';
				foreach($subitems AS $key=>$oneitem):
					if(trim($oneitem['act'])=='') $oneitem_url = ''; else $oneitem_url = 'admin.php?act='.$oneitem['act'];
					$subitems_html.=' <li><a '.($oneitem_url=='' ? '' : 'href="admin.php?act='.$oneitem['act'] ).'">'.htmlspecialchars($oneitem['title']).'</a></li> ';
				endforeach;
			$subitems_html.= ' </ul> ';
		endif;
	    
            if($act == $temp1_act)
            {
?>				
			<li class="selected" ><a <? if($temp1_url!='') echo ' href="'.$temp1_url.'" '; ?> class="top"><?= $temp1_title; ?></a>
			<?= $subitems_html ?>
			</li>
<?					  
            }
            else
			{
				?>
				<li ><a <? if($temp1_url!='') echo ' href="'.$temp1_url.'" '; ?> class="top"><?= $temp1_title; ?></a><?= $subitems_html ?></li>
				<?
			}
            
        }

        
        function DrawAdminHeaderEnd()
        {
?>            
    </ul>
  </div>
  </div>
<?
        }
        
        
        function DrawAdminHeadRow2Start()
        {
?>            
<TABLE
      style="BORDER-RIGHT: #345560 1px solid; BORDER-LEFT: #345560 1px solid; BORDER-BOTTOM: #345560 1px solid"
      height=22 cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR style="BACKGROUND-COLOR: #8498a3">
<?        
        }
        
        function DrawAdminHeadRow2Item($item)
        {
?>
            <TD style="PADDING-LEFT: 5px; COLOR: #ffffff;" vAlign=center>
            <?= $item; ?>
            </td>
<?            
        }
        
        function DrawAdminHeadRow2End()
        {
?>            
<TD style="PADDING-LEFT: 5px; COLOR: #ffffff" vAlign=center>&nbsp;</td>

                  </TR></TBODY></TABLE>

                  </TD></TR>
</TBODY></TABLE>
<?            
        }


        //Начало блока контента
        function DrawAdminContentBegin()
        {
?>
<div id="content">

      <div class="box">
    <div class="heading">
     
     
    </div>
    <div class="content">
<?
        }
        
        
        
        //Конец блока контента
        function DrawAdminContentEnd()
        {
?>
    </div>
  </div>
</div>
</div>
<?
        }
        
        
        
        function DrawAdminFooter()
        {
?>            
<div id="footer">
	<? echo date("Y",time()); ?>
</div>
</body></html>
<?
        }


		/////////////////////////////////////////
		function DrawIcon($iconname,$addparam='')
		{
			$ret = '';
			
			if($iconname=='on') $ret = '<img src="'.$this->path.'adminfiles/on.gif" border="0" '.$addparam.'>';
			if($iconname=='off') $ret = '<img src="'.$this->path.'adminfiles/off.gif" border="0" '.$addparam.'>';
			if($iconname=='up') $ret = '<img src="'.$this->path.'adminfiles/b_up.gif" border="0" '.$addparam.'>';
			if($iconname=='down') $ret = '<img src="'.$this->path.'adminfiles/b_doun.gif" border="0" '.$addparam.'>';
			if($iconname=='add') $ret = '<img src="'.$this->path.'adminfiles/add.png" border="0" '.$addparam.'>';
			if($iconname=='edit') $ret = '<img src="'.$this->path.'adminfiles/b_edit.png" border="0" '.$addparam.'>';
			if($iconname=='del') $ret = '<img src="'.$this->path.'adminfiles/b_drop.png" border="0" '.$addparam.'>';

			if($iconname=='asc') $ret = '<img src="'.$this->path.'adminfiles/asc.png" border="0" '.$addparam.'>';
			if($iconname=='desc') $ret = '<img src="'.$this->path.'adminfiles/desc.png" border="0" '.$addparam.'>';
			
			if($iconname=='save') $ret = '<img src="'.$this->path.'adminfiles/save.png" border="0" '.$addparam.'>';
			if($iconname=='xls') $ret = '<img src="'.$this->path.'adminfiles/xls.png" border="0" '.$addparam.'>';
			if($iconname=='info') $ret = '<img src="'.$this->path.'adminfiles/information.png" border="0" '.$addparam.'>';
			
			return $ret;
		}
			
    
    }

?>