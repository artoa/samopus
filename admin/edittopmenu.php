<?
	class TopmenuAdminModule Extends BasicAdminModule
	{
		var $moduleact = "edittopmenu";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'hide, title, titleh1, url, seo,spec, text, titletitle, titledescription, titlekeywords';
		var $fields_list_str = 'title';
		
		var $pagestr = 'menu'; //префикс url для модуля
		var $maxlevel = 1;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->topmenutable;
            $this->hints['maincategories'] = 'Меню';
            $this->fields['spec'] = Array('fieldtype'=>'int', 'visualtype'=>'checkbox', 'fieldhint'=>'Не показывать пункт в главном меню?', 'multilang'=>false,  'tdname'=>'Сп', 'listeditable'=>'true',);

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>