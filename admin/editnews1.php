<?
	class News1AdminModule Extends BasicAdminModule
	{
		var $moduleact = "editnews1";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'hide, title, titleh1, url, seo, date, pic, shorttext, text, titletitle, titledescription, titlekeywords';
		var $fields_list_str = 'pic, date, title';
		
		var $pagestr = 'articles'; //префикс url для модуля
		var $maxlevel = 1;
		var $itemsorder = 'prior DESC'; 
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->news1table;

			$this->fields['pic'] = Array('fieldtype'=>'text', 'visualtype'=>'image', 'fieldhint'=>'Фото', 'multilang'=>false,  'tdname'=>'Фото' , 'list_td_width'=>100, 
						'pics' => Array( 
								'params' => Array(
									Array('picprefix'=>'fotos/news1_sm_', 'w'=>500, 'h'=>300, 'mode'=>'bigsize','ext'=>'jpg'),
									Array('picprefix'=>'fotos/news1_bg_', 'w'=>600, 'h'=>400, 'mode'=>'same','ext'=>'jpg'),
								)
							),
					      );
			
			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>