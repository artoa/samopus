//var admhints= new Array();
var hintText=$('<div id="hintText"></div>').mouseleave(function(){
	hideHint()
}).mouseover(function(){
	clearInterval(hideTimer);
});
var hintHolder=$('<div id="hintHolder"></div>');
var winWidth=0;
var hintSpeed=150;
var hintTimer;
var hideTimer;
/*
admhints[0]=['hoverHint']
//admhints[1]=['ajsaver','Сохранение текущей страницы без её закрытия']
admhints[1]=['_id_hint','Сохранение текущей страницы без её закрытия']
admhints[2]=['h_addsubmenu','Добавить подраздел']
admhints[3]=['h_makehide','Скрыть']
admhints[4]=['h_makeshow','Не скрывать']
admhints[5]=['h_moveup','Переместить вверх']
admhints[6]=['h_movedown','Переместить вниз']
admhints[7]=['h_edit','Редактировать']
admhints[8]=['h_delete','Удалить']
admhints[9]=['h_comentedit','Смотреть комментарии']
admhints[10]=['h_gosub','Смотреть содержимое']
*/
function showHint(text,element){
	var nl,nt;
	if (typeof(element)==='undefined') element=hintHolder;
	off=element.offset();
	nl=off.left;nt=off.top;
	hintText.css({left:-9999});
	hintText.html(text);
	if (off.left+element.innerWidth()+hintText.innerWidth()>winWidth) nl=off.left-hintText.innerWidth();
	else nl=off.left+element.innerWidth();
	hintText.fadeOut(0);
	hintText.css({left:nl,top:nt}).fadeIn(hintSpeed)
}
function hideHint(){
	clearInterval(hintTimer);
	hintText.fadeOut(hintSpeed);
}
function initHints(){
	$('body').append(hintText).append(hintHolder);
	$(admhints).each(function(){
		var el=this;
		$('.'+el[0]).hover(function(){
			clearInterval(hintTimer);
			clearInterval(hideTimer);
			var h=this;
			hintTimer=setTimeout(function(){
				if(typeof($(h).data('hint'))!=='undefined')
					showHint($(h).data('hint'),$(h));
				else if (typeof(el[1])!=='undefined')
					showHint(el[1],$(h));
			},500)
		},function(){
			var h=this;
			hideTimer=setTimeout(function(){
				hideHint();
			},200)
		})
	})
	$(window).scroll(function(){
			hideHint();		
	});
}
/*MAIN ADMIN SCRIPTS*/
$(document).ready(function(){
	initHints();
	$(window).resize();
})


$(window).resize(function(){
	winWidth=$(window).width();
})

