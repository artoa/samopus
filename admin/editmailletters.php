<?
	class MaillettersAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editmailletters";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'hide, date, title, text';
		var $fields_list_str = 'title, date';
		
		var $pagestr = ''; //префикс url для модуля
		var $maxlevel = 1;
                
                var $itemsorder = "prior DESC";

		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->mailletterstable;
                        
                        $this->fields['hide']['fieldhint'] = 'Не отправлять письмо';
                        $this->fields['hide']['fieldhelp'] = '_hint_mailletterhide';
			
                        $this->fields['date']['fieldhint'] = 'Дата рассылки';
                        $this->fields['date']['tdname'] = 'Дата рассылки';
                        $this->fields['date']['fieldhelp'] = '_hint_mailletterdate';
			

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>