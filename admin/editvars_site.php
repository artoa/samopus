<?
	class Vars_SiteAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editvars_site";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = '
		    favicon, headcode, countercode, hidesite, comingsoon, titleprefix, titlesuffix,
		    ';
		var $fields_list_str = '';
		
		var $pagestr = ''; //префикс url для модуля
		var $maxlevel = 1;
		var $candelete = false;
		
		var $one_record_module = true; //если true - то модуль будет обсулуживать только 1 запись в базе данных
		
	
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->varstable;
			
			
			$this->fields['hidesite'] = Array('fieldtype'=>'int', 'visualtype'=>'checkbox', 'fieldhint'=>'Отключить сайт<br>(сайт будут видеть только администраторы)', 'multilang'=>false,  'fieldhelp'=>'_hint_hidesite',);
			$this->fields['comingsoon'] = Array('fieldtype'=>'text', 'visualtype'=>'fck', 'fieldhint'=>'Заставка (отображается когда сайт отключен)', 'multilang'=>false,  'fieldhelp'=>'_hint_comingsoon',);
			$this->fields['page404'] = Array('fieldtype'=>'text', 'visualtype'=>'fck', 'fieldhint'=>'Текст страницы 404', 'multilang'=>false,  'fieldhelp'=>'_hint_page404',);
			$this->fields['headcode'] = Array('fieldtype'=>'text', 'visualtype'=>'textarea', 'fieldhint'=>'Коды в блок &lt;head&gt;&lt;/head&gt;', 'multilang'=>false, 'fieldhelp'=>'_hint_headcode', );
			$this->fields['countercode'] = Array('fieldtype'=>'text', 'visualtype'=>'textarea', 'fieldhint'=>'Коды счетчиков (выводится в подвал)', 'multilang'=>false, 'fieldhelp'=>'_hint_countercode', );

			$this->fields['titleprefix'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Префикс-TITLE', 'multilang'=>true,  'fieldhelp'=>'_hint_titleprefix',);
			$this->fields['titlesuffix'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'TITLE-суффикс', 'multilang'=>true,  'fieldhelp'=>'_hint_titlesuffix',);

			$this->fields['favicon'] = Array('fieldtype'=>'text', 'visualtype'=>'image', 'fieldhint'=>'Favicon', 'multilang'=>false,  'tdname'=>'Fav' , 'fieldhelp'=>'_hint_favicon',
					'pics' => Array( 
							'params' => Array(
								Array('picprefix'=>'fotos/favicon', 'w'=>32, 'h'=>32, 'mode'=>'same','ext'=>'jpg', 'idmode'=>'withoutid'),
							)
						),
				      );
			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>