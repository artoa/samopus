<?
	class News2AdminModule Extends BasicAdminModule
	{
		var $moduleact = "editnews2";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'hide, title, titleh1, url, seo, date, pic, shorttext, text, titletitle, titledescription, titlekeywords';
		var $fields_list_str = 'pic, date, title';
		
		var $pagestr = 'articles'; //префикс url для модуля
		var $maxlevel = 2;
		var $itemsorder = 'prior DESC';
		var $itemsorderarr = Array(1=>'prior ASC');
		var $fields_level_limit = 'date: 1 '; //какие поля не показывать на каких уровнях вложенности
		

		function __construct($params=null)
		{
		    global $par;
		    $this->tablename  = $par->news2table;

		    $this->fields['pic'] = Array('fieldtype'=>'text', 'visualtype'=>'image', 'fieldhint'=>'Фото', 'multilang'=>false,  'tdname'=>'Фото' , 'list_td_width'=>100, 
					    'pics' => Array( 
							    'params' => Array(
								    Array('picprefix'=>'fotos/news2_sm_', 'w'=>300, 'h'=>200, 'mode'=>'bigsize','ext'=>'jpg'),
								    Array('picprefix'=>'fotos/news2_bg_', 'w'=>600, 'h'=>400, 'mode'=>'bigsize','ext'=>'jpg'),
							    )
						    ),
					  );
		    
		    parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>