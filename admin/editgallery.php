<?
	class GalleryAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editgallery";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'hide, title, pic';
		var $fields_list_str = 'pic, title';
		
		var $pagestr = ''; //префикс url для модуля
		var $maxlevel = 1;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->gallerytable;
            $this->hints['maincategories'] = 'Слайдер';
            $this->fields['title']['multilang'] = true;
			$this->fields['pic'] = Array('fieldtype'=>'text', 'visualtype'=>'image', 'fieldhint'=>'Картинка', 'multilang'=>false,  'tdname'=>'Картинка' , 'list_td_width'=>100,
						    'pics' => Array( 
								    'params' => Array(
									    Array('picprefix'=>'fotos/slide_bg_', 'w'=>260, 'h'=>290, 'mode'=>'bigsize','ext'=>'jpg'),
									    Array('picprefix'=>'fotos/slide_sm_', 'w'=>260, 'h'=>290, 'mode'=>'same','ext'=>'jpg')
								    )
							    ),
						  );
			

			
			

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>