<?
class CallAdminModule Extends BasicAdminModule
{
    var $moduleact = "editcall";  //act для модуля
    var $tablename = ""; //таблицу переопределить в конструкторе

    var $fields_str = 'date, phone,orderstatus';
    var $fields_list_str = 'date,phone,orderstatus';

    var $pagestr = ''; //префикс url для модуля
    var $maxlevel = 1;
    var $itemsorder = "date DESC"; //ASC or DESC порядок сортировки пунктов в админке
    var $canhide = false;
    function __construct($params=null)
    {
        global $par;
        $this->tablename  = $par->calltable;
        $this->hints['maincategories'] = 'Обратный звонок';
        $this->fields['phone'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'tdname'=>'Телефон','fieldhint'=>'Телефон', 'multilang'=>false,  );
        $this->fields['orderstatus'] = Array('fieldtype'=>'int', 'visualtype'=>'select', 'selecttable'=>'orderstatuses', 'selecttablefield'=>'title', 'selectorderfield'=>'id ASC', 'selectmaxlevel'=>1, 'fieldhint'=>'Статус сообщения', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStatus', 'tdname'=>'Статус', 'listeditable'=>true, 'width'=>150, 'list_td_width'=>150, );

        parent::__construct($params);
    }

    function MetodListStatus($field_key,$field_value,$suffix,$line)
    {
        global $par;

        $width = $this->inputtextwidth;
        if(isset($field_value['width'])) $width = $field_value['width'];

        $lang = '';
        if($field_value['multilang']==true) $lang = ' '.$this->GetLangByValue($suffix,'suffix',$mode='admlangstitle');

        $tdname = '';
        if(isset($field_value['tdname'])) $tdname = $field_value['tdname'].$lang;

        $sorturl = '';
        if(!isset($field_value['sortable']) || $field_value['sortable']==true) $sorturl = 'workadmin.php?act=changesortorder&moduleact='.$this->moduleact.'&sortfield='.$field_key.$suffix;

        $ascdesc = '';
        if(isset($_SESSION['sortorder:'.$this->moduleact]) && $_SESSION['sortorder:'.$this->moduleact]==$field_key) $ascdesc = 'asc';
        if(isset($_SESSION['sortorder:'.$this->moduleact]) && $_SESSION['sortorder:'.$this->moduleact]==$field_key." DESC ") $ascdesc = 'desc';

        $value = '';

        if($field_value['visualtype']=='select')
        {
            $tid = (int)$line[$field_key.$suffix];


            //***********************************
            if(isset($field_value['listeditable']) && $field_value['listeditable']==true)
            {
                global $__tabindex__;
                if(isset($__tabindex__)) $tabindex++;
                else $tabindex = 1;
                $t = str_replace('`','',$this->tablename);
                $t2 = '__edit__::' .$t.'::'. $field_key . '::' . $line['id'];

                $currarr = Array( );
                $currarr[] = $this->GetFieldValue($line,$field_key,$field_value['fieldtype']);
                $value .= '<select class="listeditablefield" name="'.$t2.'" style="width:'.$width.'px;" id="'.$t2.'"  style="width:'.$width.'px; "><option value="0"></option>';
                $value .=$this->ReturnOptions($field_value['selecttable'], $field_value['selecttablefield'], $field_value['selectorderfield'], 0, $currarr,$field_value['selectmaxlevel'], 0);
                $value .= '</select>';
            }
            else
            {
                $sqltmp = "SELECT * FROM ".$field_value['selecttable']." WHERE id=".$tid;
                $restmp = mysql_query($sqltmp);
                if($linetmp = mysql_fetch_array($restmp,MYSQL_ASSOC))
                {
                    $value = htmlspecialchars($linetmp[$field_value['selecttablefield']]);
                }
            }
        }

        $a = Array('type'=>'td', 'value'=>$value, 'tdname'=>$tdname, 'sorturl'=>$sorturl, 'ascdesc'=>$ascdesc, 'list_td_width'=>$field_value['list_td_width'], );
        //asdfsdasdfdf

        $a['td_color'] = GetStatusColor($line[$field_key]);
        if(isset($field_value['listeditable']) && $field_value['listeditable']==true)
        {
            $a['listeditable'] = 'true';
        }
        return Array($a);


    }


}

////////////////////////////////////////////////////////////////////////////////

?>