<?
	class FilialsAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editfilials";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'hide, title, titleh1, url, seo, map, shorttext, text, titletitle, titledescription, titlekeywords';
		var $fields_list_str = 'title';
		
		var $pagestr = 'filials'; //префикс url для модуля
		var $maxlevel = 3;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->filialstable;
			
			$this->fields['shorttext']['fieldhint'] = 'Информация для значка на карте';
			$this->fields['shorttext']['visualtype'] = 'input';

			$this->fields['map'] = Array('fieldtype'=>'none', 'visualtype'=>'map',   /*настройки карты*/ 'paramarr'=>Array( 'mapprovider'=>'yandex', 'default_lng'=>37.627038415521405, 'default_lat'=>55.745429334465435 , 'default_zoom'=>5 , /*По умолчанию координаты Москвы*/  'map_width'=>600, 'map_height'=>400,  'prefix_lng'=>'lng',  'prefix_lat'=>'lat',  'prefix_zoom'=>'zoom', /*префикс в названии полей в таблице у lat, lng, zoom*/  ),   /*конец настроек карты*/   'fieldhint'=>'Карта', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStandart', 'sortable'=>false,/*Если не нужна сортировка по этому полю в списке*/ 'tdname'=>'Карта', 'listeditable'=>false, );
			
			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>