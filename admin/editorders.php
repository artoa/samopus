<?
	class OrdersAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editorders";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'id, email, name, address, phone, allsum, orderstatus, text, ';
		var $fields_list_str = 'id, email, name, phone, address, orderstr, allsum, text,  orderstatus';
		
		var $pagestr = ''; //префикс url для модуля
		var $maxlevel = 1;
		var $itemsorder = "id DESC";
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->orderstable;

			$this->fields['userid'] = Array('fieldtype'=>'int', 'visualtype'=>'input', 'fieldhint'=>'userid', 'multilang'=>false,  'tdname'=>'userid', );
			
			$this->fields['email'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'E-mail', 'multilang'=>false,  'tdname'=>'E-mail', 'insertmode'=>true, 'filter'=>Array('filtertype'=>'text', 'filterwidth'=>80),);
			$this->fields['name'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Имя', 'multilang'=>false,  'tdname'=>'Имя', 'insertmode'=>true, 'filter'=>Array('filtertype'=>'text', 'filterwidth'=>80), );
			$this->fields['address'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Адрес', 'multilang'=>false,  'tdname'=>'Адрес', 'insertmode'=>true, 'filter'=>Array('filtertype'=>'text', 'filterwidth'=>80), );
			$this->fields['phone'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Телефон', 'multilang'=>false,  'tdname'=>'Телефон', 'insertmode'=>true, 'filter'=>Array('filtertype'=>'text', 'filterwidth'=>80),);

			$this->fields['text'] = Array('fieldtype'=>'text', 'visualtype'=>'textarea', 'fieldhint'=>'Текст', 'multilang'=>false,  'tdname'=>'Комментарий',  );
			//$this->fields['ordertext'] = Array('fieldtype'=>'text', 'visualtype'=>'none', 'fieldhint'=>'Комментарий', 'multilang'=>false,  'tdname'=>'Комментарий', );

			$this->fields['orderstr'] = Array('fieldtype'=>'text', 'visualtype'=>'none', 'fieldhint'=>'Описание заказа', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListOrderStr', 'tdname'=>'Заказ', );

			$this->fields['allsum'] = Array('fieldtype'=>'double', 'visualtype'=>'input', 'fieldhint'=>'Сумма', 'multilang'=>false,  'tdname'=>'Сумма', 'filter'=>Array('filtertype'=>'int', 'filterwidth'=>50),);
			//$this->fields['orderstatus'] = Array('fieldtype'=>'int', 'visualtype'=>'input', 'fieldhint'=>'Статус', 'multilang'=>false,  'tdname'=>'Статус', );
			$this->fields['orderstatus'] = Array('fieldtype'=>'int', 'visualtype'=>'select', 'selecttable'=>'orderstatuses', 'selecttablefield'=>'title', 'selectorderfield'=>'id ASC', 'selectmaxlevel'=>1, 'fieldhint'=>'Статус', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStatus', 'tdname'=>'Статус', 'listeditable'=>true, 'width'=>150, 'list_td_width'=>150, );
			
			parent::__construct($params);
		}
		
		function MetodListStatus($field_key,$field_value,$suffix,$line)
		{
		    global $par;
		    
		    $width = $this->inputtextwidth;
		    if(isset($field_value['width'])) $width = $field_value['width'];
		    
		    $lang = '';
		    if($field_value['multilang']==true) $lang = ' '.$this->GetLangByValue($suffix,'suffix',$mode='admlangstitle');
		    
		    $tdname = '';
		    if(isset($field_value['tdname'])) $tdname = $field_value['tdname'].$lang;
		    
		    $sorturl = '';
		    if(!isset($field_value['sortable']) || $field_value['sortable']==true) $sorturl = 'workadmin.php?act=changesortorder&moduleact='.$this->moduleact.'&sortfield='.$field_key.$suffix;
		    
		    $ascdesc = '';
		    if(isset($_SESSION['sortorder:'.$this->moduleact]) && $_SESSION['sortorder:'.$this->moduleact]==$field_key) $ascdesc = 'asc';
		    if(isset($_SESSION['sortorder:'.$this->moduleact]) && $_SESSION['sortorder:'.$this->moduleact]==$field_key." DESC ") $ascdesc = 'desc';
		    
		    $value = '';
		    
			if($field_value['visualtype']=='select')
			{
				$tid = (int)$line[$field_key.$suffix];
				
				
				//***********************************
				if(isset($field_value['listeditable']) && $field_value['listeditable']==true)
				{
					global $__tabindex__;
					if(isset($__tabindex__)) $tabindex++;
					else $tabindex = 1;
					$t = str_replace('`','',$this->tablename);
					$t2 = '__edit__::' .$t.'::'. $field_key . '::' . $line['id'];
					
					$currarr = Array( );
					$currarr[] = $this->GetFieldValue($line,$field_key,$field_value['fieldtype']);
					$value .= '<select class="listeditablefield" name="'.$t2.'" style="width:'.$width.'px;" id="'.$t2.'"  style="width:'.$width.'px; "><option value="0"></option>';
					$value .=$this->ReturnOptions($field_value['selecttable'], $field_value['selecttablefield'], $field_value['selectorderfield'], 0, $currarr,$field_value['selectmaxlevel'], 0);
					$value .= '</select>';
				}
				else
				{
					$sqltmp = "SELECT * FROM ".$field_value['selecttable']." WHERE id=".$tid;
					$restmp = mysql_query($sqltmp);
					if($linetmp = mysql_fetch_array($restmp,MYSQL_ASSOC))
					{
						$value = htmlspecialchars($linetmp[$field_value['selecttablefield']]);
					}
				}
			}

		    $a = Array('type'=>'td', 'value'=>$value, 'tdname'=>$tdname, 'sorturl'=>$sorturl, 'ascdesc'=>$ascdesc, 'list_td_width'=>$field_value['list_td_width'], );
		    //asdfsdasdfdf
		    
		    $a['td_color'] = GetStatusColor($line[$field_key]);
		    if(isset($field_value['listeditable']) && $field_value['listeditable']==true)
		    {
			    $a['listeditable'] = 'true';
		    }
		    return Array($a);
		    
		
		}
		
		function MetodListOrderStr($field_key,$field_value,$suffix,$line)
		{
//			$a = Array('type'=>'td', 'value'=>$value, 'tdname'=>$tdname, 'sorturl'=>$sorturl, 'ascdesc'=>$ascdesc);
			
			$tdname = '';
			if(isset($field_value['tdname'])) $tdname = $field_value['tdname'];
			
			$sorturl = '';

			$value = '';
			//$value .= htmlspecialchars($this->GetFieldValue($line,$field_key.$suffix,$field_value['fieldtype']));
			$orderarr = GetOrderInfo($line['id'],$line);
			
			
			$value.= '<a href="ordertoxls.php?id='.$line['id'].'" title="Импорт в Excel">'.$this->adm->DrawIcon('xls',' width="32" ').'</a>';

			$value.= '
			<br>
			
			';

    			if($this->GetAdmConfig($this->moduleact,'hideordertable'))
			{
			    $value.= '
				<a href="#" onclick=" document.getElementById(\'_ordertable_id_'.$line['id'].'\').style.display = \'block\'; return false; ">Показать таблицу заказа</a>
				<div style="display:none;" id="_ordertable_id_'.$line['id'].'">';
			}
			
			$value.= '
			<a href="#" onclick=" document.location.href = \'workadmin.php?act=changeadmconfig&modulename='.$this->moduleact.'&var=hideordertable&value=0\'; return false;">Всегда показывать таблицу заказа</a><br>
			<a href="#" onclick=" document.location.href = \'workadmin.php?act=changeadmconfig&modulename='.$this->moduleact.'&var=hideordertable&value=1\'; return false;">Всегда скрывать таблицу заказа</a><br>
			
			<style>
			    .admin_order_table .admin_order_table th, .admin_order_table td { border:1px solid #c7c7c7; border-collapse:collapse; padding:0px; }
			</style>
			';
			
			$value.='<table class="admin_order_table">';
			$value.='<tr><td>Фото</td><td>(ID)</td><td>АРТ</td><td>Товар</td><td>Цена</td><td>К-во</td><td>Сумма</td></tr>';
			foreach($orderarr['products'] AS $key=>$oneproduct)
			{
			    if(isset($oneproduct['isdeleted']))
			    {
				$oneproduct['id'] = ' ';
				$oneproduct['artikul'] = ' ';
				$oneproduct['fname'] = '';
				$oneproduct['url'] = '';
				$oneproduct['title'] = 'Товар удален из базы';				
				$oneproduct['id'] = '';
			    }
			    else
			    {
				$addstr = GetAddStr(100,75,$oneproduct['fname']);
			    }
			    
			    $resprice = $oneproduct['_order_value'] * $oneproduct['_order_price'];
			    
			    $value.='
			    <tr>
				<td>'.($oneproduct['fname']=='' ? ' ' : '<a href="'.$oneproduct['url'].'"><img src="/'.$oneproduct['fname'].'" '.$addstr.'></a>').'</td>
				<td>'.$oneproduct['id'].'</td>
				<td>'.htmlspecialchars($oneproduct['artikul']).'</td>
				<td><a '.($oneproduct['url']=='' ? '' : ' href="'.$oneproduct['url'].'"').'>'.$oneproduct['title'].'</a></td>
				<td>'.$oneproduct['_order_price'].'</td>
				<td>'.$oneproduct['_order_value'].'</td>
				<td>'.PriceToStr($resprice).'</td>
			    </tr>
			    ';
			}
			$value.='</table>';
    			if($this->GetAdmConfig($this->moduleact,'hideordertable')) $value.= '</div>';
			
			$a = Array('type'=>'td', 'value'=>$value, 'tdname'=>$tdname, 'sorturl'=>$sorturl, );
			return Array($a);
		    
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>