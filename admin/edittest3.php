<?
	class Test3AdminModule Extends BasicAdminModule
	{
		var $moduletype = 'objectstype'; //тип модуля товары - он относится к каталогу
		
		var $moduleact = "edittest3";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе

		var $cattablename = ""; //таблица для категорий - определить в конструкторе
		var $catmoduleact = "edittest1"; //act для модуля категорий
		
		var $fields_str = 'id, categid, title, ';
		var $fields_list_str = 'id, title';
		
		var $pagestr = 'test3'; //префикс url для модуля
		var $maxlevel = 1;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = 'test3';
			$this->cattablename  = 'test1';

			$this->fields['title']['metodlist'] = 'MetodListStandart';
			
			$this->fields['categid'] = Array('fieldtype'=>'int',   /*для select*/ 'visualtype'=>'select',  'selecttable'=>'category', 'selecttablefield'=>'title', 'selectorderfield'=>'id ASC', 'selectmaxlevel'=>4,/*end - для select*/          'fieldhint'=>'Категория', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStandart', 'tdname'=>'Категория');
					

			$this->itemsorder = "prior DESC"; //ASC or DESC порядок сортировки пунктов в админке
			$this->hints['maincategories'] = 'CATTEST3';

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>