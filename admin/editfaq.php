<?
	class FaqAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editfaq";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'hide, title, titleh1, url, seo, text, answer, author, titletitle, titledescription, titlekeywords';
		var $fields_list_str = 'title';
		
		var $pagestr = 'faq'; //префикс url для модуля
		var $maxlevel = 2;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->faqtable;
			
			$this->fields['text']['visualtype'] = 'fck';
			$this->fields['text']['fieldhint'] = 'Вопрос';
			
			$this->fields['answer']  = Array('fieldtype'=>'text', 'visualtype'=>'fck', 'fieldhint'=>'Ответ', 'multilang'=>true,  'sortable'=>false,/*Если не нужна сортировка по этому полю в списке*/ 'tdname'=>'Ответ');
			$this->fields['author']  = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Автор', 'multilang'=>true,  'sortable'=>false,/*Если не нужна сортировка по этому полю в списке*/ 'tdname'=>'Автор');
			

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>