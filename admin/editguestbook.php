<?
class GuestbookAdminModule Extends BasicAdminModule
{
    var $moduleact = "editguestbook";  //act для модуля
    var $tablename = ""; //таблицу переопределить в конструкторе

    var $fields_str = 'hide, date,pic,title,shorttext, text,orderstatus,titletitle, titlekeywords, titledescription';
    var $fields_list_str = 'pic,date,title,orderstatus';

    var $pagestr = 'guestbook'; //префикс url для модуля
    var $maxlevel = 1;

    function __construct($params=null)
    {
        global $par;
        $this->tablename  = $par->guestbooktable;
        $this->fields['orderstatus'] = Array('fieldtype'=>'int', 'visualtype'=>'select', 'selecttable'=>'orderstatuses', 'selecttablefield'=>'title', 'selectorderfield'=>'id ASC', 'selectmaxlevel'=>1, 'fieldhint'=>'Статус сообщения', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStatus', 'tdname'=>'Статус', 'listeditable'=>true, 'width'=>150, 'list_td_width'=>150, );

        $this->fields['pic'] = Array('fieldtype'=>'text', 'visualtype'=>'image', 'fieldhint'=>'Фото', 'multilang'=>false,  'tdname'=>'Фото' , 'list_td_width'=>100,
            'pics' => Array(
                'params' => Array(
                    Array('picprefix'=>'fotos/guestbook_sm_', 'w'=>500, 'h'=>300, 'mode'=>'bigsize','ext'=>'jpg'),
                    Array('picprefix'=>'fotos/guestbook_bg_', 'w'=>600, 'h'=>400, 'mode'=>'same','ext'=>'jpg'),
                )
            ),
        );
        $this->hints['maincategories'] = 'Все отзывы';

        $this->fields['title']['multilang'] = false;
        $this->fields['text']['multilang'] = false;
        $this->fields['shorttext']['multilang'] = false;

        $this->fields['text']['fieldhint'] = 'Текст отзыва';

        $this->fields['spec'] = Array('fieldtype'=>'int', 'visualtype'=>'checkbox', 'fieldhint'=>'Вывести на главной', 'multilang'=>false,  'tdname'=>'Сп',);


        $this->fields['name'] = Array('fieldtype'=>'text', 'insertmode'=>false,'visualtype'=>'input', 'tdname'=>'Имя','fieldhint'=>'Имя', 'multilang'=>false,  );

        parent::__construct($params);
    }
    function MetodListStatus($field_key,$field_value,$suffix,$line)
    {
        global $par;

        $width = $this->inputtextwidth;
        if(isset($field_value['width'])) $width = $field_value['width'];

        $lang = '';
        if($field_value['multilang']==true) $lang = ' '.$this->GetLangByValue($suffix,'suffix',$mode='admlangstitle');

        $tdname = '';
        if(isset($field_value['tdname'])) $tdname = $field_value['tdname'].$lang;

        $sorturl = '';
        if(!isset($field_value['sortable']) || $field_value['sortable']==true) $sorturl = 'workadmin.php?act=changesortorder&moduleact='.$this->moduleact.'&sortfield='.$field_key.$suffix;

        $ascdesc = '';
        if(isset($_SESSION['sortorder:'.$this->moduleact]) && $_SESSION['sortorder:'.$this->moduleact]==$field_key) $ascdesc = 'asc';
        if(isset($_SESSION['sortorder:'.$this->moduleact]) && $_SESSION['sortorder:'.$this->moduleact]==$field_key." DESC ") $ascdesc = 'desc';

        $value = '';

        if($field_value['visualtype']=='select')
        {
            $tid = (int)$line[$field_key.$suffix];


            //***********************************
            if(isset($field_value['listeditable']) && $field_value['listeditable']==true)
            {
                global $__tabindex__;
                if(isset($__tabindex__)) $tabindex++;
                else $tabindex = 1;
                $t = str_replace('`','',$this->tablename);
                $t2 = '__edit__::' .$t.'::'. $field_key . '::' . $line['id'];

                $currarr = Array( );
                $currarr[] = $this->GetFieldValue($line,$field_key,$field_value['fieldtype']);
                $value .= '<select class="listeditablefield" name="'.$t2.'" style="width:'.$width.'px;" id="'.$t2.'"  style="width:'.$width.'px; "><option value="0"></option>';
                $value .=$this->ReturnOptions($field_value['selecttable'], $field_value['selecttablefield'], $field_value['selectorderfield'], 0, $currarr,$field_value['selectmaxlevel'], 0);
                $value .= '</select>';
            }
            else
            {
                $sqltmp = "SELECT * FROM ".$field_value['selecttable']." WHERE id=".$tid;
                $restmp = mysql_query($sqltmp);
                if($linetmp = mysql_fetch_array($restmp,MYSQL_ASSOC))
                {
                    $value = htmlspecialchars($linetmp[$field_value['selecttablefield']]);
                }
            }
        }

        $a = Array('type'=>'td', 'value'=>$value, 'tdname'=>$tdname, 'sorturl'=>$sorturl, 'ascdesc'=>$ascdesc, 'list_td_width'=>$field_value['list_td_width'], );
        //asdfsdasdfdf

        $a['td_color'] = GetStatusColor($line[$field_key]);
        if(isset($field_value['listeditable']) && $field_value['listeditable']==true)
        {
            $a['listeditable'] = 'true';
        }
        return Array($a);


    }


}

////////////////////////////////////////////////////////////////////////////////

?>