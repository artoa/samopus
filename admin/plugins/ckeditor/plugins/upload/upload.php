<?php
	@session_start();

        if(isset($_SESSION['logadmin']) && $_SESSION['logadmin']==1)
	{
		
	}
	else
	{
		$callback = $_REQUEST['CKEditorFuncNum'];
		echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'.$callback.'", "","ВОЙДИТЕ ПОД АДМИНОМ" );</script>';
		exit();
	}

	//функция для конвертирования кириллических имен файлов
	function cleanFilename($filename) 
	{
		$good_letters = Array();
		for($c='a';$c<='z';$c++) $good_letters[] = $c;
		for($c='A';$c<='Z';$c++) $good_letters[] = $c;
		for($c='0';$c<='9';$c++) $good_letters[] = $c;
		$good_letters[] = '-';
		$good_letters[] = '_';
		$good_letters[] = '(';
		$good_letters[] = ')';
		$good_letters[] = '[';
		$good_letters[] = ']';
		$good_letters[] = '{';
		$good_letters[] = '}';
		$good_letters[] = '.';

		$n_filename="";

		$trans = array("а"=>"a","б"=>"b","в"=>"v","г"=>"g", "д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j", "з"=>"z","и"=>"i","й"=>"i","к"=>"k", "л"=>"l","м"=>"m","н"=>"n","о"=>"o", "п"=>"p","р"=>"r","с"=>"s","т"=>"t", "у"=>"u","ф"=>"f","х"=>"h","ц"=>"c", "ч"=>"ch","ш"=>"sh","щ"=>"sh","ы"=>"i", "э"=>"e","ю"=>"u","я"=>"ya",
		"А"=>"A","Б"=>"B","В"=>"V","Г"=>"G", "Д"=>"D","Е"=>"E","Ё"=>"Yo","Ж"=>"J", "З"=>"Z","И"=>"I","Й"=>"I","К"=>"K","Л"=>"L", "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R", "С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"H", "Ц"=>"C","Ч"=>"Ch","Ш"=>"Sh","Щ"=>"Sh", "Ы"=>"I","Э"=>"E","Ю"=>"U","Я"=>"Ya",
		"ь"=>"","Ь"=>"","ъ"=>"","Ъ"=>"");
		$tmp_filename = strtr($filename, $trans);
		$filename = strtolower ($tmp_filename);

		//Check that it only contains valid characters
		for($i=0;$i<strlen($filename);$i++) if (in_array(substr($filename,$i,1),$good_letters)) $n_filename.=substr($filename,$i,1);

		//If it got this far all is ok
		return $n_filename;
	}

    $uploaddir = '../../../../../userfiles';
    $file_Name = $_FILES['upload']["name"];
	$file_Name = cleanFilename( $file_Name );
	
    $file_TmpName = $_FILES['upload']["tmp_name"];
    $uploadfile = $uploaddir .'/'. $file_Name;

	$full_path = '/userfiles/'.$file_Name;
	
	$k = 0;
	while(is_file($uploadfile))
	{
		$k++;
		$ext = pathinfo($file_Name, PATHINFO_EXTENSION);
		$uploadfile = $uploaddir .'/'.substr($file_Name, 0, strlen($file_Name)-strlen($ext)-1).'('.$k.').'.$ext;
		//$file_Name = substr($file_Name, 0, strlen($file_Name)-strlen($ext)-1).'('.$k.').'.$ext;
		$full_path = '/userfiles/'.substr($file_Name, 0, strlen($file_Name)-strlen($ext)-1).'('.$k.').'.$ext;
	}

    move_uploaded_file($file_TmpName, $uploadfile);
     
    $callback = $_REQUEST['CKEditorFuncNum'];
     
    //$full_path = '/userfiles/'.$file_Name;
	
    echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'.$callback.'", "'.$full_path.'","Файл загружен" );</script>';

?>