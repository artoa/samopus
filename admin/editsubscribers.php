<?
	class SubscribersAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editsubscribers";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'id, hide, email, firstname, date, ';
		var $fields_list_str = 'id, email, firstname, date';
		
		var $pagestr = ''; //префикс url для модуля
		var $maxlevel = 1;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->subscriberstable;
                        
                        $this->fields['hide']['fieldhint'] = 'Подписка не подтверждена';

			$this->fields['email'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'E-mail', 'multilang'=>false,  'tdname'=>'E-mail', 'insertmode'=>true, );
			$this->fields['firstname'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Имя', 'multilang'=>false,  'tdname'=>'Имя', 'insertmode'=>true, );

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>