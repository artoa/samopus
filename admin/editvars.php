<?
	class VarsAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editvars";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = '
		    adminemail, password,
		    use_smtp, smtp_host, smtp_port, smtp_ssl, smtp_username, smtp_userpass, smtp_title, 
		    ';
		var $fields_list_str = 'adminemail';
		
		var $pagestr = ''; //префикс url для модуля
		var $maxlevel = 1;
		var $candelete = false;
		
		var $one_record_module = true; //если true - то модуль будет обсулуживать только 1 запись в базе данных
		
	
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->varstable;
			$this->fields['phone_sent'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Телефон админа в формате +380001234567', 'multilang'=>false,  );
			
			$this->fields['adminemail'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'E-mail менеджера', 'multilang'=>false, 'fieldhelp'=>'_hint_adminemail', );
			$this->fields['password'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Пароль администратора', 'multilang'=>false, 'fieldhelp'=>'_hint_password', );


			$this->fields['use_smtp'] = Array('fieldtype'=>'int', 'visualtype'=>'checkbox', 'fieldhint'=>'Использовать SMTP отправку почты', 'multilang'=>false, 'fieldhelp'=>'_hint_usesmtp',);
			$this->fields['smtp_host'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'SMTP host', 'multilang'=>false, 'fieldhelp'=>'_hint_smtphost',);
			$this->fields['smtp_port'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'SMTP порт', 'multilang'=>false, 'fieldhelp'=>'_hint_smtpport',);
			$this->fields['smtp_ssl'] = Array('fieldtype'=>'int', 'visualtype'=>'checkbox', 'fieldhint'=>'SSL соединение', 'multilang'=>false, 'fieldhelp'=>'_hint_smtpssl',);
			$this->fields['smtp_username'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'SMTP логин', 'multilang'=>false, 'fieldhelp'=>'_hint_smtplogin',);
			$this->fields['smtp_userpass'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'SMTP пароль', 'multilang'=>false, 'fieldhelp'=>'_hint_smtppassword',);
			$this->fields['smtp_title'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Подпись оправителя', 'multilang'=>false, 'fieldhelp'=>'_hint_smtptitle',);

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>