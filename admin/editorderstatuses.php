<?
	class OrderStatusesAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editorderstatuses";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'title, color';
		var $fields_list_str = 'title, color';
		
		var $pagestr = ''; //префикс url для модуля
		var $maxlevel = 1;
		var $canhide = false;
		function __construct($params=null)
		{
			global $par;
            $this->hints['maincategories'] = 'Статусы сообщений';
			$this->tablename  = $par->orderstatusestable;
            $this->fields['title']['multilang'] = false;
            $this->fields['color']['multilang'] = false;
			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>