<?
class FotoAdminModule Extends BasicAdminModule
{
    var $moduleact = "editfoto";  //act для модуля
    var $tablename = ""; //таблицу переопределить в конструкторе

    var $fields_str = 'hide, title, titleh1, url, seo, tip, pic, gallery, titletitle, titledescription, titlekeywords';
    var $fields_list_str = 'pic, title';

    var $pagestr = 'gallery'; //префикс url для модуля
    var $maxlevel = 1;

    function __construct($params=null)
    {
        global $par;
        $this->tablename  = $par->fototable;
        $this->fields['text'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Подпись под обложкой альбома', 'multilang'=>true,  'sortable'=>false,/*Если не нужна сортировка по этому полю в списке*/ 'tdname'=>'Описание');

        $this->fields['tip'] = Array('fieldtype'=>'int',   /*для select*/ 'visualtype'=>'select',  'selecttable'=>'tip', 'selecttablefield'=>'title', 'selectorderfield'=>'id ASC', 'selectmaxlevel'=>1,/*end - для select*/          'fieldhint'=>'Тип', 'multilang'=>false,  'tdname'=>'Категория');

        $this->fields['pic'] = Array('fieldtype'=>'text', 'visualtype'=>'image', 'fieldhint'=>'Обложка альбома', 'multilang'=>false,  'tdname'=>'Обложка' , 'list_td_width'=>100,
            'pics' => Array(
                'params' => Array(
                    Array('picprefix'=>'fotos/gallery_cover_or_', 'w'=>1050, 'h'=>1050, 'mode'=>'bigsize','ext'=>'jpg')
                )
            ),
        );


        $this->fields['gallery'] = Array('fieldtype'=>'text', 'visualtype'=>'gallery',  'fieldhint'=>'Фото в галереи', 'multilang'=>false,  'tdname'=>'Фото в галереи',
            'gallerypics_tablename' => 'fotor',
            'gallerypics' => Array(
                Array('picprefix'=>'fotos/gallery_sm_', 'w'=>440, 'h'=>294, 'mode'=>'bigsize','ext'=>'jpg'),
                Array('picprefix'=>'fotos/gallery_bg_', 'w'=>1050, 'h'=>1050, 'mode'=>'bigsize','ext'=>'jpg')
            ),

        );


        parent::__construct($params);
    }
}
