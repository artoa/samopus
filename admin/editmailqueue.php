<?
	class MailqueueAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editmailqueue";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'hide, date, email, firstname, title, text, emailfrom ';
		var $fields_list_str = 'email, firstname, title, date';
		
		var $pagestr = ''; //префикс url для модуля
		var $maxlevel = 1;
                
                var $itemsorder = "prior ASC";

		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->mailqueuetable;
                        
			$this->fields['email'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'E-mail получателя', 'multilang'=>false,  'tdname'=>'E-mail получателя', 'insertmode'=>true, );
			$this->fields['firstname'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Имя', 'multilang'=>false,  'tdname'=>'Имя', 'insertmode'=>true, );

                        $this->fields['hide']['fieldhint'] = 'Не отправлять письмо';
                        $this->fields['hide']['fieldhelp'] = '_hint_mailletterhide';
			
                        $this->fields['date']['fieldhint'] = 'Дата посылки';
                        $this->fields['date']['tdname'] = 'Дата посылки';

                        $this->fields['text']['insertmode'] = true;

			$this->fields['emailfrom'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'E-mail отправителя', 'multilang'=>false,  'tdname'=>'E-mail отправителя', 'insertmode'=>true, );
			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>