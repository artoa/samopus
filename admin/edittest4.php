<?
	class Test4AdminModule Extends BasicAdminModule
	{
		var $moduletype = Array(
							Array('moduletype'=>'objectstype', 'cattablename'=>'test2', 'catmoduleact'=>'edittest2', 'catmodulefile'=>'edittest2.php', 'catclassname'=>'Test2AdminModule'),
							);		
							
		var $moduleact = "edittest4";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе

//		var $cattablename = ""; //таблица для категорий - определить в конструкторе
//		var $catmoduleact = "edittest2"; //act для модуля категорий
		
		var $fields_str = 'id, pic, categid, title, ';
		var $fields_list_str = 'id, pic, title';
		
		var $pagestr = 'test4'; //префикс url для модуля
		var $maxlevel = 1;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = 'test4';
			$this->cattablename  = 'test1';

			$this->fields['title']['metodlist'] = 'MetodListStandart';
			
			$this->fields['categid'] = Array('fieldtype'=>'int',   /*для select*/ 'visualtype'=>'select',  'selecttable'=>'test2', 'selecttablefield'=>'title', 'selectorderfield'=>'id ASC', 'selectmaxlevel'=>4,/*end - для select*/          'fieldhint'=>'Категория', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStandart', 'tdname'=>'Категория');
					

			$this->itemsorder = "prior DESC"; //ASC or DESC порядок сортировки пунктов в админке
			$this->hints['maincategories'] = 'Все ученики';

			$this->fields['pic'] = Array('fieldtype'=>'text', 'visualtype'=>'image', 'fieldhint'=>'Фото', 'multilang'=>false,  'tdname'=>'Фото' , 'list_td_width'=>100, 
						'pics' => Array( 
								'params' => Array(
									Array('picprefix'=>'fotos/avatar_sm_', 'w'=>300, 'h'=>200, 'mode'=>'bigsize','ext'=>'jpg'),
								)
							),
					      );
			
			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>