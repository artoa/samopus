<?
	class UsersAdminModule Extends BasicAdminModule
	{
		var $moduleact = "editusers";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		
		var $fields_str = 'id, hide, email, password, firstname, phone, address, code, lastvisitdate ';
		var $fields_list_str = 'id, email, firstname, phone, ';
		
		var $pagestr = ''; //префикс url для модуля
		var $maxlevel = 1;
		var $itemsorder = "prior DESC";
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->userstable;

			$this->fields['email'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'E-mail', 'multilang'=>false,  'tdname'=>'E-mail', 'insertmode'=>true, 'filter'=>Array('filtertype'=>'text', 'filterwidth'=>150),  );
			$this->fields['firstname'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Имя', 'multilang'=>false,  'tdname'=>'Имя', 'insertmode'=>true, 'filter'=>Array('filtertype'=>'text', 'filterwidth'=>150), );
			$this->fields['password'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Пароль', 'multilang'=>false,  'tdname'=>'Пароль', 'insertmode'=>true, );
			$this->fields['address'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Адрес', 'multilang'=>false,  'tdname'=>'Адрес', 'insertmode'=>true, 'filter'=>Array('filtertype'=>'text', 'filterwidth'=>150), );
			$this->fields['phone'] = Array('fieldtype'=>'text', 'visualtype'=>'input', 'fieldhint'=>'Телефон', 'multilang'=>false,  'tdname'=>'Телефон', 'insertmode'=>true, 'filter'=>Array('filtertype'=>'text', 'filterwidth'=>150), );
			$this->fields['code'] = Array('fieldtype'=>'text', 'visualtype'=>'none', 'fieldhint'=>'Код регистрации', 'multilang'=>false,  'tdname'=>'Код регистрации', 'disabled'=>true);
			$this->fields['lastvisitdate'] = Array('fieldtype'=>'int', 'visualtype'=>'date', 'needtime'=>true, 'fieldhint'=>'Последний визит', 'multilang'=>false,  'tdname'=>'Последний визит', 'disabled'=>true, 'fieldhelp'=>'_hint_lastvisitdate' );

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>