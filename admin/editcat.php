<?
	class CatAdminModule Extends BasicAdminModule
	{

		var $moduleact = "editcat";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		//var $catobjtable = ""; //таблица объектов данного каталога - переопределить в конструкторе

		var $moduletype = Array(
					Array('moduletype'=>'catalogtype', 'objectsmodulefile'=>'editobjects.php', 'objectstable'=>'', 'editobjectsact'=>'editobjects', 'objectsclassname'=>'ObjectsAdminModule', 'hintobjects'=>'Товары', ),
				    );
		
		var $fields_str = 'title, titleh1, url, seo, text, titletitle, titlekeywords, titledescription';
		var $fields_list_str = 'title';
		
		var $pagestr = 'cat'; //префикс url для модуля
		var $maxlevel = 2;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = $par->categorytable;
			$this->moduletype[0]['objectstable'] = $par->objectstable;
			
			$this->hints['maincategories'] = 'Все категории';
			
			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>