<?
	class Test1AdminModule Extends BasicAdminModule
	{

		var $moduleact = "edittest1";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе
		//var $catobjtable = ""; //таблица объектов данного каталога - переопределить в конструкторе

        //var $moduletype = "catalogtype"; //тип модуля каталог (у него будут объекты)
		
		var $moduletype = Array(
							Array('moduletype'=>'catalogtype', 'objectsmodulefile'=>'edittest2.php', 'objectstable'=>'test2', 'editobjectsact'=>'edittest2', 'objectsclassname'=>'Test2AdminModule', 'hintobjects'=>'Школы'),
							Array('moduletype'=>'catalogtype', 'objectsmodulefile'=>'edittest3.php', 'objectstable'=>'test3', 'editobjectsact'=>'edittest3', 'objectsclassname'=>'Test3AdminModule', 'hintobjects'=>'Магазины'),
							);

		var $objectsmodulefile = "edittest2.php"; // var $objectsmodulefile = "editobjects.php"; путь к файлу с класом объектов категории
		var $objectstable = ""; // аблица объектов данного каталога - переопределить в конструкторе
        var $editobjectsact = 'edittest2';
		var $objectsclassname = 'Test2AdminModule'; //название класса модуля объектов (в editobjects.php или аналоге)

                
		var $fields_str = 'title';
		var $fields_list_str = 'title';
		
		var $pagestr = 'test1'; //префикс url для модуля
		var $maxlevel = 2;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = 'test1';
			$this->objectstable = 'test2';
			
			$this->fields['title']['metodlist'] = 'MetodListStandart';
			$this->hints['maincategories'] = 'ВСЕ ГОРОДА';
			
			
			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>