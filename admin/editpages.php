<?
class PagesAdminModule Extends BasicAdminModule
{
    var $moduleact = "editpages";  //act для модуля
    var $tablename = ""; //таблицу переопределить в конструкторе

    var $fields_str = 'title, titleh1, url, seo, text, titletitle, titledescription, titlekeywords';
    var $fields_list_str = 'title';

    var $pagestr = 'pages'; //префикс url для модуля
    var $maxlevel = 1;

    function __construct($params=null)
    {
        global $par;
        $this->hints['maincategories'] = 'Доп. страницы';
        $this->tablename  = $par->pagestable;

        parent::__construct($params);
    }
}

////////////////////////////////////////////////////////////////////////////////

?>