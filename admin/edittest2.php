<?
	class Test2AdminModule Extends BasicAdminModule
	{
		//var $moduletype = 'objectstype'; //тип модуля товары - он относится к каталогу
		var $moduletype = Array(
							Array('moduletype'=>'objectstype', 'cattablename'=>'test1', 'catmoduleact'=>'edittest1', 'catmodulefile'=>'edittest1.php', 'catclassname'=>'Test1AdminModule'),
							Array('moduletype'=>'catalogtype', 'objectsmodulefile'=>'edittest4.php', 'objectstable'=>'test4', 'editobjectsact'=>'edittest4', 'objectsclassname'=>'Test4AdminModule', 'hintobjects'=>'Ученики'),
							);
		
		var $moduleact = "edittest2";  //act для модуля
		var $tablename = ""; //таблицу переопределить в конструкторе

	//	var $cattablename = ""; //таблица для категорий - определить в конструкторе
		
		var $fields_str = 'id, categid, title, ';
		var $fields_list_str = 'id, title';
		
		var $pagestr = 'test2'; //префикс url для модуля
		var $maxlevel = 1;
		
		function __construct($params=null)
		{
			global $par;
			$this->tablename  = 'test2';
		//	$this->cattablename  = 'test1';

			$this->fields['title']['metodlist'] = 'MetodListStandart';
			
			$this->fields['categid'] = Array('fieldtype'=>'int',   /*для select*/ 'visualtype'=>'select',  'selecttable'=>'test1', 'selecttablefield'=>'title', 'selectorderfield'=>'id ASC', 'selectmaxlevel'=>4,/*end - для select*/          'fieldhint'=>'Категория', 'multilang'=>false, 'metod'=>'MetodFormStandart', 'metodedit'=>'MetodEditStandart', 'metodlist'=>'MetodListStandart', 'tdname'=>'Категория');
					

			$this->itemsorder = "prior DESC"; //ASC or DESC порядок сортировки пунктов в админке

			$this->hints['maincategories'] = 'Все школы';

			parent::__construct($params);
		}
	}
	
    ////////////////////////////////////////////////////////////////////////////////    
	
?>